<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productions', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('title', 255)->nullable();
            $table->longText('description')->nullable();
            $table->longText('feature')->nullable();
            $table->longText('technical')->nullable();
            $table->longText('content')->nullable();
            $table->string('image_path', 255)->nullable();
            $table->tinyInteger('sort')->default(1);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productions');
    }
}
