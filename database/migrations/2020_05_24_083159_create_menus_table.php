<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('title', 255)->nullable();
            $table->tinyInteger('type')->default(0)->comment('1:Solution, 2:Product, 3:News, 4: Article');
            $table->tinyInteger('ref_id')->nullable();
            $table->boolean('active')->default(1);
            $table->boolean('show_top')->default(0);
            $table->boolean('show_footer')->default(0);
            $table->tinyInteger('sort')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
