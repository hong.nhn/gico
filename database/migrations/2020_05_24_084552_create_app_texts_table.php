<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_texts', function (Blueprint $table) {
            $table->id();
            $table->mediumText('name');
            $table->string('link', 255)->nullable();
            $table->string('image_path', 255)->nullable();
            $table->tinyInteger('type')->default(0)->comment('1:header, 2:footer, 3:top');
            $table->boolean('active')->default(1);;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_texts');
    }
}
