-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for gico
CREATE DATABASE IF NOT EXISTS `gico` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gico`;

-- Dumping structure for table gico.app_texts
CREATE TABLE IF NOT EXISTS `app_texts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:header, 2:footer, 3:top',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.app_texts: ~17 rows (approximately)
/*!40000 ALTER TABLE `app_texts` DISABLE KEYS */;
INSERT INTO `app_texts` (`id`, `name`, `link`, `image_path`, `type`, `active`, `created_at`, `updated_at`) VALUES
	(1, '<span>c</span>ông trình tiêu biểu', NULL, NULL, 3, 1, '2020-06-03 13:18:56', '2020-06-03 13:19:23'),
	(2, 'giải pháp<span>iot</span>', NULL, NULL, 3, 1, '2020-06-03 13:20:54', '2020-06-03 13:20:54'),
	(3, '<span>s</span>ản phẩm', NULL, NULL, 3, 1, '2020-06-03 13:41:13', '2020-06-03 13:41:13'),
	(4, '<span>đ</span>ánh giá từ khách hàng', NULL, NULL, 3, 1, '2020-06-03 14:01:24', '2020-06-03 14:01:24'),
	(5, 'facebook', 'facebook.com', '<i class="fab fa-facebook-f"></i>', 1, 1, '2020-06-09 13:30:14', '2020-06-12 15:07:54'),
	(6, 'logo', '/', 'images/icon/logo_slogun_white_icon_1024.png', 2, 1, '2020-06-09 14:11:23', '2020-06-16 13:48:14'),
	(7, '+84 915437070', 'tel:+84 915437070', 'images/icon/phone.svg', 2, 1, '2020-06-09 14:19:04', '2020-06-09 14:31:23'),
	(8, '+84 918795454', 'tel:+84 918795454', 'images/icon/phone.svg', 2, 1, '2020-06-09 14:25:54', '2020-06-09 14:31:37'),
	(9, 'gico.jsc@gmail.com', 'mailto:gico.jsc@gmail.com', 'images/icon/mail.svg', 2, 1, '2020-06-09 14:31:13', '2020-06-09 14:35:00'),
	(10, 'info@gico.tech', 'mailto:info@gico.tech', 'images/icon/mail.svg', 2, 1, '2020-06-09 14:36:34', '2020-06-09 14:36:34'),
	(11, 'centana tower a11.3, 36 mai chi tho st., an phu ward, dist.2, hcm city, vn', NULL, 'images/icon/marker.svg', 2, 1, '2020-06-09 14:38:00', '2020-06-09 14:38:00'),
	(12, 'copyright © 2000 - 2020 gico co.,ltd. all rights reserved', NULL, NULL, 2, 1, '2020-06-09 14:40:53', '2020-06-09 14:40:53'),
	(13, 'youtube', 'youtube.com', '<i class="fab fa-youtube"></i>', 1, 1, '2020-06-12 13:15:30', '2020-06-12 15:09:51'),
	(14, 'mail', 'mailto:gico.jsc@gmail.com', '<i class="far fa-envelope"></i>', 1, 1, '2020-06-12 13:18:37', '2020-06-12 15:10:55'),
	(15, '+84 915437070', 'tel:+84 915437070', '<i class="fa fa-phone" aria-hidden="true"></i>', 1, 1, '2020-06-12 13:20:09', '2020-06-12 13:21:10'),
	(16, 'logo', '/', 'images/icon/logo_slogun.png', 1, 1, '2020-06-12 13:24:44', '2020-06-12 13:24:44'),
	(19, 'banner', NULL, 'front/images/banner/index.jpg', 3, 1, '2020-06-12 13:35:36', '2020-06-12 13:35:36'),
	(20, 'solution background', NULL, 'images/uploads/index/solutions_bg.jpg', 3, 1, '2020-06-12 13:45:39', '2020-06-12 13:49:33'),
	(21, 'customer rating background', NULL, 'images/uploads/index/green_background.png', 3, 1, '2020-06-12 13:53:36', '2020-06-12 13:55:49');
/*!40000 ALTER TABLE `app_texts` ENABLE KEYS */;

-- Dumping structure for table gico.articles
CREATE TABLE IF NOT EXISTS `articles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` tinyint(4) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.articles: ~3 rows (approximately)
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` (`id`, `name`, `title`, `description`, `content`, `image_path`, `template`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'liên hệ', 'liên hệ', NULL, '<div class="title--with--line">\r\n                    Map\r\n                </div>\r\n                <div class="page--content text-justify">\r\n                    <div class="shadown">\r\n                        <div id="company_map"></div>\r\n                    </div>\r\n                </div>\r\n                <div class="title--with--line">\r\n                    Thông tin liên hệ\r\n                </div>\r\n                <div class="page--content">\r\n                    <p>\r\n                        <a href="#" class="btn btn-outline-warning gico-color mr-5">\r\n                            TEL: +84 915437070\r\n                        </a>\r\n                        <a href="#" class="btn btn-outline-warning gico-color mr-5">\r\n                            TEL: +84 918795454\r\n                        </a>\r\n                    </p>\r\n                    <p>\r\n                        <a href="#" class="btn btn-outline-danger gico-color mr-5">\r\n                            MAIL: gico.jsc@gmail.com\r\n                        </a>\r\n                        <a href="#" class="btn btn-outline-danger gico-color mr-5">\r\n                            MAIL: info@gico.tech\r\n                        </a>\r\n\r\n                    </p>\r\n                    <p>\r\n                        ADDRESS: Centana Tower A11.3, 36 Mai Chi Tho st., An Phu Ward, Dist.2, HCM City, VN\r\n                    </p>\r\n                </div>', NULL, 1, 1, '2020-06-03 14:17:18', '2020-06-03 14:17:18'),
	(2, 'giới thiệu', 'giới thiệu', NULL, '<div class="title--with--line">\r\n                    Tổng quan công ty\r\n                </div>\r\n                <div class="page--content text-justify">\r\n                    <p>GICO là một Công ty Công nghệ nghiên cứu phát triển các sản phẩm, giải pháp trong lĩnh vực kết nối vạn vật (Internet of Things – IoT) và trí tuệ nhân tạo (AI). Chiến lược phát triển công nghệ của GICO được cố vấn từ các chuyên gia kỹ thuật đang công tác tại Trường Đại học Quốc gia TPHCM. Đội ngũ kỹ sư của GICO có trình độ chuyên môn cao hội tụ từ các Doanh nghiệp trong lĩnh vực Công nghệ thông tin và truyền thông, có cùng tâm huyết xây dựng và phát triển các sản phẩm, giải pháp công nghệ thông minh cho toàn thế giới đến từ Việt Nam.</p>\r\n                    <p>Dựa trên nền tảng Internet of Things (IoT) và chiến lược kinh doanh đa dạng, GICO phát triển các giải pháp và sản phẩm công nghệ phục vụ cho doanh nghiệp/cá nhân tham gia trong các ngành du lịch, dịch vụ bán lẻ, giáo dục, phát triển bất động sản, nông nghiệp công nghệ cao, … Chính vì vậy, mọi thành viên của công ty luôn luôn ý thức và có trách nhiệm cao trong từng chi tiết sản phẩm công nghệ do GICO thiết kế ra. Tất cả vì mục tiêu đem đến cho khách hàng sự an tâm, tin tưởng và hài lòng nhất khi sử dụng các sản phẩm và giải pháp của GICO.</p>\r\n                    <p>Với thông điệp mang công nghệ thông minh của kỹ sư Việt Nam đến toàn thế giới giúp mọi người có cuộc sống tiện lợi hơn, GICO mong muốn đem đến các sản phẩm, giải pháp thông minh sử dụng các thuật toán trí tuệ nhân tạo nhằm phục vụ cho những người dẫn đầu xu hướng chia sẻ trong thời đại mới.</p>\r\n                </div>', NULL, NULL, 1, '2020-06-03 14:27:28', '2020-06-03 14:27:28'),
	(3, 'privacy policy', 'privacy policy', NULL, '<h1>Privacy Policy</h1>\r\n<p>Last updated: June 02, 2020</p>\r\n<p>This Privacy Policy describes Our policies and procedures on the collection, use and disclosure of Your information when You use the Service and tells You about Your privacy rights and how the law protects You.</p>\r\n<p>We use Your Personal data to provide and improve the Service. By using the Service, You agree to the collection and use of information in accordance with this Privacy Policy. This Privacy Policy has been created with the help of the <a href="https://www.termsfeed.com/privacy-policy-generator/" target="_blank">Privacy Policy Generator</a>.</p>\r\n<h1>Interpretation and Definitions</h1>\r\n<h2>Interpretation</h2>\r\n<p>The words of which the initial letter is capitalized have meanings defined under the following conditions.\r\nThe following definitions shall have the same meaning regardless of whether they appear in singular or in plural.</p>\r\n<h2>Definitions</h2>\r\n<p>For the purposes of this Privacy Policy:</p>\r\n<ul>\r\n<li>\r\n<p><strong>You</strong> means the individual accessing or using the Service, or the company, or other legal entity on behalf of which such individual is accessing or using the Service, as applicable.</p>\r\n</li>\r\n<li>\r\n<p><strong>Company</strong> (referred to as either "the Company", "We", "Us" or "Our" in this Agreement) refers to GICO JSC, Ho Chi Minh City.</p>\r\n</li>\r\n<li>\r\n<p><strong>Affiliate</strong> means an entity that controls, is controlled by or is under common control with a party, where "control" means ownership of 50% or more of the shares, equity interest or other securities entitled to vote for election of directors or other managing authority.</p>\r\n</li>\r\n<li>\r\n<p><strong>Account</strong> means a unique account created for You to access our Service or parts of our Service.</p>\r\n</li>\r\n<li>\r\n<p><strong>Website</strong> refers to GICO TECH, accessible from <a href="https://gico.tech" rel="external nofollow noopener" target="_blank">https://gico.tech</a></p>\r\n</li>\r\n<li>\r\n<p><strong>Service</strong> refers to the Website.</p>\r\n</li>\r\n<li>\r\n<p><strong>Country</strong> refers to:  Vietnam</p>\r\n</li>\r\n<li>\r\n<p><strong>Service Provider</strong> means any natural or legal person who processes the data on behalf of the Company. It refers to third-party companies or individuals employed by the Company to facilitate the Service, to provide the Service on behalf of the Company, to perform services related to the Service or to assist the Company in analyzing how the Service is used.</p>\r\n</li>\r\n<li>\r\n<p><strong>Third-party Social Media Service</strong> refers to any website or any social network website through which a User can log in or create an account to use the Service.</p>\r\n</li>\r\n<li>\r\n<p><strong>Personal Data</strong> is any information that relates to an identified or identifiable individual.</p>\r\n</li>\r\n<li>\r\n<p><strong>Cookies</strong> are small files that are placed on Your computer, mobile device or any other device by a website, containing the details of Your browsing history on that website among its many uses.</p>\r\n</li>\r\n<li>\r\n<p><strong>Device</strong> means any device that can access the Service such as a computer, a cellphone or a digital tablet.</p>\r\n</li>\r\n<li>\r\n<p><strong>Usage Data</strong> refers to data collected automatically, either generated by the use of the Service or from the Service infrastructure itself (for example, the duration of a page visit).</p>\r\n</li>\r\n</ul>\r\n<h1>Collecting and Using Your Personal Data</h1>\r\n<h2>Types of Data Collected</h2>\r\n<h3>Personal Data</h3>\r\n<p>While using Our Service, We may ask You to provide Us with certain personally identifiable information that can be used to contact or identify You. Personally identifiable information may include, but is not limited to:</p>\r\n<ul>\r\n<li>\r\n<p>Email address</p>\r\n</li>\r\n<li>\r\n<p>First name and last name</p>\r\n</li>\r\n<li>\r\n<p>Phone number</p>\r\n</li>\r\n<li>\r\n<p>Address, State, Province, ZIP/Postal code, City</p>\r\n</li>\r\n<li>\r\n<p>Usage Data</p>\r\n</li>\r\n</ul>\r\n<h3>Usage Data</h3>\r\n<p>Usage Data is collected automatically when using the Service.</p>\r\n<p>Usage Data may include information such as Your Device\'s Internet Protocol address (e.g. IP address), browser type, browser version, the pages of our Service that You visit, the time and date of Your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>\r\n<p>When You access the Service by or through a mobile device, We may collect certain information automatically, including, but not limited to, the type of mobile device You use, Your mobile device unique ID, the IP address of Your mobile device, Your mobile operating system, the type of mobile Internet browser You use, unique device identifiers and other diagnostic data.</p>\r\n<p>We may also collect information that Your browser sends whenever You visit our Service or when You access the Service by or through a mobile device.</p>\r\n<h3>Tracking Technologies and Cookies</h3>\r\n<p>We use Cookies and similar tracking technologies to track the activity on Our Service and store certain information. Tracking technologies used are beacons, tags, and scripts to collect and track information and to improve and analyze Our Service.</p>\r\n<p>You can instruct Your browser to refuse all Cookies or to indicate when a Cookie is being sent. However, if You do not accept Cookies, You may not be able to use some parts of our Service.</p>\r\n<p>Cookies can be "Persistent" or "Session" Cookies. Persistent Cookies remain on your personal computer or mobile device when You go offline, while Session Cookies are deleted as soon as You close your web browser. Learn more about cookies: <a href="https://www.termsfeed.com/blog/cookies/" target="_blank">All About Cookies</a>.</p>\r\n<p>We use both session and persistent Cookies for the purposes set out below:</p>\r\n<ul>\r\n<li>\r\n<p><strong>Necessary / Essential Cookies</strong></p>\r\n<p>Type: Session Cookies</p>\r\n<p>Administered by: Us</p>\r\n<p>Purpose: These Cookies are essential to provide You with services available through the Website and to enable You to use some of its features. They help to authenticate users and prevent fraudulent use of user accounts. Without these Cookies, the services that You have asked for cannot be provided, and We only use these Cookies to provide You with those services.</p>\r\n</li>\r\n<li>\r\n<p><strong>Cookies Policy / Notice Acceptance Cookies</strong></p>\r\n<p>Type: Persistent Cookies</p>\r\n<p>Administered by: Us</p>\r\n<p>Purpose: These Cookies identify if users have accepted the use of cookies on the Website.</p>\r\n</li>\r\n<li>\r\n<p><strong>Functionality Cookies</strong></p>\r\n<p>Type: Persistent Cookies</p>\r\n<p>Administered by: Us</p>\r\n<p>Purpose: These Cookies allow us to remember choices You make when You use the Website, such as remembering your login details or language preference. The purpose of these Cookies is to provide You with a more personal experience and to avoid You having to re-enter your preferences every time You use the Website.</p>\r\n</li>\r\n</ul>\r\n<p>For more information about the cookies we use and your choices regarding cookies, please visit our Cookies Policy or the Cookies section of our Privacy Policy.</p>\r\n<h2>Use of Your Personal Data</h2>\r\n<p>The Company may use Personal Data for the following purposes:</p>\r\n<ul>\r\n<li><strong>To provide and maintain our Service</strong>, including to monitor the usage of our Service.</li>\r\n<li><strong>To manage Your Account:</strong> to manage Your registration as a user of the Service. The Personal Data You provide can give You access to different functionalities of the Service that are available to You as a registered user.</li>\r\n<li><strong>For the performance of a contract:</strong> the development, compliance and undertaking of the purchase contract for the products, items or services You have purchased or of any other contract with Us through the Service.</li>\r\n<li><strong>To contact You:</strong> To contact You by email, telephone calls, SMS, or other equivalent forms of electronic communication, such as a mobile application\'s push notifications regarding updates or informative communications related to the functionalities, products or contracted services, including the security updates, when necessary or reasonable for their implementation.</li>\r\n<li><strong>To provide You</strong> with news, special offers and general information about other goods, services and events which we offer that are similar to those that you have already purchased or enquired about unless You have opted not to receive such information.</li>\r\n<li><strong>To manage Your requests:</strong> To attend and manage Your requests to Us.</li>\r\n</ul>\r\n<p>We may share your personal information in the following situations:</p>\r\n<ul>\r\n<li><strong>With Service Providers:</strong> We may share Your personal information with Service Providers to monitor and analyze the use of our Service,  to contact You.</li>\r\n<li><strong>For Business transfers:</strong> We may share or transfer Your personal information in connection with, or during negotiations of, any merger, sale of Company assets, financing, or acquisition of all or a portion of our business to another company.</li>\r\n<li><strong>With Affiliates:</strong> We may share Your information with Our affiliates, in which case we will require those affiliates to honor this Privacy Policy. Affiliates include Our parent company and any other subsidiaries, joint venture partners or other companies that We control or that are under common control with Us.</li>\r\n<li><strong>With Business partners:</strong> We may share Your information with Our business partners to offer You certain products, services or promotions.</li>\r\n<li><strong>With other users:</strong> when You share personal information or otherwise interact in the public areas with other users, such information may be viewed by all users and may be publicly distributed outside. If You interact with other users or register through a Third-Party Social Media Service, Your contacts on the Third-Party Social Media Service may see Your name, profile, pictures and description of Your activity. Similarly, other users will be able to view descriptions of Your activity, communicate with You and view Your profile.</li>\r\n</ul>\r\n<h2>Retention of Your Personal Data</h2>\r\n<p>The Company will retain Your Personal Data only for as long as is necessary for the purposes set out in this Privacy Policy. We will retain and use Your Personal Data to the extent necessary to comply with our legal obligations (for example, if we are required to retain your data to comply with applicable laws), resolve disputes, and enforce our legal agreements and policies.</p>\r\n<p>The Company will also retain Usage Data for internal analysis purposes. Usage Data is generally retained for a shorter period of time, except when this data is used to strengthen the security or to improve the functionality of Our Service, or We are legally obligated to retain this data for longer time periods.</p>\r\n<h2>Transfer of Your Personal Data</h2>\r\n<p>Your information, including Personal Data, is processed at the Company\'s operating offices and in any other places where the parties involved in the processing are located. It means that this information may be transferred to — and maintained on — computers located outside of Your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from Your jurisdiction.</p>\r\n<p>Your consent to this Privacy Policy followed by Your submission of such information represents Your agreement to that transfer.</p>\r\n<p>The Company will take all steps reasonably necessary to ensure that Your data is treated securely and in accordance with this Privacy Policy and no transfer of Your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of Your data and other personal information.</p>\r\n<h2>Disclosure of Your Personal Data</h2>\r\n<h3>Business Transactions</h3>\r\n<p>If the Company is involved in a merger, acquisition or asset sale, Your Personal Data may be transferred. We will provide notice before Your Personal Data is transferred and becomes subject to a different Privacy Policy.</p>\r\n<h3>Law enforcement</h3>\r\n<p>Under certain circumstances, the Company may be required to disclose Your Personal Data if required to do so by law or in response to valid requests by public authorities (e.g. a court or a government agency).</p>\r\n<h3>Other legal requirements</h3>\r\n<p>The Company may disclose Your Personal Data in the good faith belief that such action is necessary to:</p>\r\n<ul>\r\n<li>Comply with a legal obligation</li>\r\n<li>Protect and defend the rights or property of the Company</li>\r\n<li>Prevent or investigate possible wrongdoing in connection with the Service</li>\r\n<li>Protect the personal safety of Users of the Service or the public</li>\r\n<li>Protect against legal liability</li>\r\n</ul>\r\n<h2>Security of Your Personal Data</h2>\r\n<p>The security of Your Personal Data is important to Us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While We strive to use commercially acceptable means to protect Your Personal Data, We cannot guarantee its absolute security.</p>\r\n<h1>Your California Privacy Rights (California\'s Shine the Light law)</h1>\r\n<p>Under California Civil Code Section 1798 (California\'s Shine the Light law), California residents with an established business relationship with us can request information once a year about sharing their Personal Data with third parties for the third parties\' direct marketing purposes.</p>\r\n<p>If you\'d like to request more information under the California Shine the Light law, and if you are a California resident, You can contact Us using the contact information provided below.</p>\r\n<h1>California Privacy Rights for Minor Users (California Business and Professions Code Section 22581)</h1>\r\n<p>California Business and Professions Code section 22581 allow California residents under the age of 18 who are registered users of online sites, services or applications to request and obtain removal of content or information they have publicly posted.</p>\r\n<p>To request removal of such data, and if you are a California resident, You can contact Us using the contact information provided below, and include the email address associated with Your account.</p>\r\n<p>Be aware that Your request does not guarantee complete or comprehensive removal of content or information posted online and that the law may not permit or require removal in certain circumstances.</p>\r\n<h1>Links to Other Websites</h1>\r\n<p>Our Service may contain links to other websites that are not operated by Us. If You click on a third party link, You will be directed to that third party\'s site. We strongly advise You to review the Privacy Policy of every site You visit.</p>\r\n<p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>\r\n<h1>Changes to this Privacy Policy</h1>\r\n<p>We may update our Privacy Policy from time to time. We will notify You of any changes by posting the new Privacy Policy on this page.</p>\r\n<p>We will let You know via email and/or a prominent notice on Our Service, prior to the change becoming effective and update the "Last updated" date at the top of this Privacy Policy.</p>\r\n<p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>\r\n<h1>Contact Us</h1>\r\n<p>If you have any questions about this Privacy Policy, You can contact us:</p>\r\n<ul>\r\n<li>\r\n<p>By email: info@gico.tech</p>\r\n</li>\r\n<li>\r\n<p>By visiting this page on our website: <a href="https://gico.tech" rel="external nofollow noopener" target="_blank">https://gico.tech</a></p>\r\n</li>\r\n<li>\r\n<p>By phone number: +84915437070</p>\r\n</li>\r\n</ul>', NULL, NULL, 1, '2020-06-03 14:34:04', '2020-06-03 14:34:04');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

-- Dumping structure for table gico.constructions
CREATE TABLE IF NOT EXISTS `constructions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.constructions: ~4 rows (approximately)
/*!40000 ALTER TABLE `constructions` DISABLE KEYS */;
INSERT INTO `constructions` (`id`, `name`, `image_path`, `sort`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'căn hộ khu đô thị vạn phúc', 'images/uploads/construction/15911901913.png', 1, 1, '2020-06-03 13:16:31', '2020-06-03 13:16:31'),
	(2, 'nhà mẫu goldsand hill villa , mũi né', 'images/uploads/construction/159119020466.png', 1, 1, '2020-06-03 13:16:44', '2020-06-03 13:16:44'),
	(3, 'căn hộ landmark 81', 'images/uploads/construction/159119021544.png', 1, 1, '2020-06-03 13:16:55', '2020-06-03 13:16:55'),
	(4, 'căn hộ centana, thủ thiêm 2', 'images/uploads/construction/159352619485.png', 1, 1, '2020-06-03 13:17:08', '2020-06-30 14:09:54');
/*!40000 ALTER TABLE `constructions` ENABLE KEYS */;

-- Dumping structure for table gico.customer_ratings
CREATE TABLE IF NOT EXISTS `customer_ratings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `star` tinyint(4) NOT NULL DEFAULT '1',
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `sort` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.customer_ratings: ~3 rows (approximately)
/*!40000 ALTER TABLE `customer_ratings` DISABLE KEYS */;
INSERT INTO `customer_ratings` (`id`, `name`, `star`, `content`, `sort`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'NGUYỄN VĂN A', 5, '<p>Some quick example text to build on the card title and make up the bulk of the card\'s content.</p><p>Some quick example text to build on the card title and make up the bulk of the card\'s content.<br></p>', 1, 1, '2020-06-03 13:43:22', '2020-06-03 13:43:22'),
	(2, 'NGUYỄN VĂN B', 5, '<p>Some quick example text to build on the card title and make up the bulk of the card\'s content.<br></p>', 1, 1, '2020-06-03 13:43:33', '2020-06-03 13:43:33'),
	(3, 'NGUYỄN VĂN C', 5, '<p><br></p>', 1, 1, '2020-06-03 13:43:50', '2020-06-14 13:57:33');
/*!40000 ALTER TABLE `customer_ratings` ENABLE KEYS */;

-- Dumping structure for table gico.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.failed_jobs: ~0 rows (approximately)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table gico.image_uploads
CREATE TABLE IF NOT EXISTS `image_uploads` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.image_uploads: ~0 rows (approximately)
/*!40000 ALTER TABLE `image_uploads` DISABLE KEYS */;
INSERT INTO `image_uploads` (`id`, `image_path`, `created_at`, `updated_at`) VALUES
	(1, 'images/uploads/1591709407.png', '2020-06-09 13:30:07', '2020-06-09 13:30:07');
/*!40000 ALTER TABLE `image_uploads` ENABLE KEYS */;

-- Dumping structure for table gico.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1:Solution, 2:Product, 3:News, 4: Article',
  `ref_id` tinyint(4) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show_top` tinyint(1) NOT NULL DEFAULT '0',
  `show_footer` tinyint(1) NOT NULL DEFAULT '0',
  `sort` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.menus: ~9 rows (approximately)
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `name`, `title`, `type`, `ref_id`, `active`, `show_top`, `show_footer`, `sort`, `created_at`, `updated_at`) VALUES
	(1, 'trang chủ', NULL, 4, NULL, 1, 1, 1, 1, '2020-06-03 13:56:23', '2020-06-16 13:50:34'),
	(2, 'giới thiệu', NULL, 4, 2, 1, 1, 1, 2, '2020-06-03 13:57:33', '2020-06-14 14:11:53'),
	(3, 'giải pháp thông minh', NULL, 1, NULL, 1, 1, 1, 3, '2020-06-03 13:57:45', '2020-06-14 14:11:34'),
	(4, 'thiết bị thông minh', NULL, 2, NULL, 1, 1, 1, 4, '2020-06-03 13:57:57', '2020-06-14 14:11:44'),
	(5, 'tin tức', NULL, 3, NULL, 1, 1, 0, 5, '2020-06-03 13:58:06', '2020-06-14 12:44:37'),
	(6, 'liên hệ', NULL, 4, 1, 1, 1, 1, 6, '2020-06-03 13:58:15', '2020-06-14 14:12:02'),
	(7, 'privacy policy', NULL, 4, 3, 1, 0, 1, 7, '2020-06-03 14:32:46', '2020-06-14 14:12:15'),
	(8, 'menu a', NULL, 4, NULL, 1, 0, 0, 1, '2020-06-30 14:11:23', '2020-06-30 14:11:23'),
	(9, 'menu b', NULL, 2, NULL, 1, 0, 0, 1, '2020-06-30 14:11:54', '2020-06-30 14:17:02');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- Dumping structure for table gico.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.migrations: ~13 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(42, '2020_05_24_083103_create_nodes_table', 2),
	(43, '2020_05_24_083159_create_menus_table', 2),
	(44, '2020_05_24_083215_create_solutions_table', 2),
	(45, '2020_05_24_084552_create_app_texts_table', 2),
	(46, '2020_05_27_093520_create_image_uploads_table', 2),
	(47, '2020_05_29_071930_create_customer_ratings_table', 2),
	(48, '2020_06_01_134613_create_news_table', 2),
	(49, '2020_06_01_134909_create_articles_table', 2),
	(50, '2020_06_03_015452_create_productions_table', 2),
	(51, '2020_06_03_015717_create_constructions_table', 2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table gico.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.news: ~2 rows (approximately)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` (`id`, `name`, `description`, `content`, `image_path`, `sort`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'fresh review of coming trends for summer ‘15', '<p><span style="color: rgb(128, 128, 128); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Class aptent taciti sociosqu ad litora torquent…</span><br></p>', '<p><span style="color: rgb(128, 128, 128); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";">Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, eiusmod tempor incididunt ut labore et dolore magna aliqua. Class aptent taciti sociosqu ad litora torquent…</span><br></p>', 'images/uploads/news/15934382662.png', 1, 1, '2020-06-14 12:40:21', '2020-06-29 13:44:26'),
	(2, 'kết quả tìm kiếm kết quả tìm kiếm trên web  tin tức 24h mới nhất, tin nhanh, tin nóng hà', '<h1 class="bNg8Rb" style="margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-family: arial, sans-serif; font-size: 14px; clip: rect(1px, 1px, 1px, 1px); height: 1px; overflow: hidden; position: absolute; white-space: nowrap; width: 1px; z-index: -1000; color: rgb(34, 34, 34);">Kết quả tìm kiếm</h1><div eid="9yHmXtb4LYjAz7sP8biTiAU" data-async-context="query:tin%20tuc" id="rso" style="margin-top: 6px; color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: medium;"><div class="g" style="line-height: 1.2; width: 600px; font-size: 14px; margin-top: 0px; margin-bottom: 28px;"><h2 class="bNg8Rb" style="clip: rect(1px, 1px, 1px, 1px); height: 1px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; overflow: hidden; padding: 0px; position: absolute; white-space: nowrap; width: 1px; z-index: -1000;">Kết quả tìm kiếm trên web</h2><div class="rc" data-hveid="CAMQAA" data-ved="2ahUKEwiW0LresIHqAhUI4HMBHXHcBFEQFSgAMAB6BAgDEAA" style="position: relative;"><div class="r" style="margin: 0px; font-size: small; line-height: 1.58;"><a href="https://thanhnien.vn/" ping="/url?sa=t&amp;source=web&amp;rct=j&amp;url=https://thanhnien.vn/&amp;ved=2ahUKEwiW0LresIHqAhUI4HMBHXHcBFEQFjAAegQIAxAB" style="color: rgb(102, 0, 153); cursor: pointer;"><br><h3 class="LC20lb DKV0Md" style="font-size: 20px; margin-right: 0px; margin-bottom: 3px; margin-left: 0px; padding: 4px 0px 0px; display: inline-block; line-height: 1.3;">Tin tức 24h mới nhất, tin nhanh, tin nóng hà</h3><p style="font-size: 20px; margin-right: 0px; margin-bottom: 3px; margin-left: 0px; padding: 4px 0px 0px; display: inline-block; line-height: 1.3;"><br></p></a></div><h1 class="bNg8Rb" style="margin-right: 0px; margin-bottom: 0px; margin-left: 0px; padding: 0px; font-family: arial, sans-serif; font-size: 14px; clip: rect(1px, 1px, 1px, 1px); height: 1px; overflow: hidden; position: absolute; white-space: nowrap; width: 1px; z-index: -1000; color: rgb(34, 34, 34);">Kết quả tìm kiếm</h1><div eid="9yHmXtb4LYjAz7sP8biTiAU" data-async-context="query:tin%20tuc" id="rso" style="margin-top: 6px; font-size: medium;"><div class="g" style="line-height: 1.2; width: 600px; font-size: 14px; margin-top: 0px; margin-bottom: 28px;"><h2 class="bNg8Rb" style="clip: rect(1px, 1px, 1px, 1px); height: 1px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; overflow: hidden; padding: 0px; position: absolute; white-space: nowrap; width: 1px; z-index: -1000;">Kết quả tìm kiếm trên web</h2><div class="rc" data-hveid="CAMQAA" data-ved="2ahUKEwiW0LresIHqAhUI4HMBHXHcBFEQFSgAMAB6BAgDEAA" style="position: relative;"><div class="r" style="margin: 0px; font-size: small; line-height: 1.58;"><a href="https://thanhnien.vn/" ping="/url?sa=t&amp;source=web&amp;rct=j&amp;url=https://thanhnien.vn/&amp;ved=2ahUKEwiW0LresIHqAhUI4HMBHXHcBFEQFjAAegQIAxAB" style="color: rgb(102, 0, 153); cursor: pointer;"><br><h3 class="LC20lb DKV0Md" style="font-size: 20px; margin-right: 0px; margin-bottom: 3px; margin-left: 0px; padding: 4px 0px 0px; display: inline-block; line-height: 1.3;">Tin tức 24h mới nhất, tin nhanh, tin nóng hà</h3></a></div></div></div></div></div></div></div>', NULL, 'images/uploads/news/159214030159.jpg', 2, 1, '2020-06-14 13:11:41', '2020-06-14 13:11:49');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;

-- Dumping structure for table gico.nodes
CREATE TABLE IF NOT EXISTS `nodes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nodeable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nodeable_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nodes_nodeable_type_nodeable_id_index` (`nodeable_type`,`nodeable_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.nodes: ~24 rows (approximately)
/*!40000 ALTER TABLE `nodes` DISABLE KEYS */;
INSERT INTO `nodes` (`id`, `name`, `slug`, `nodeable_type`, `nodeable_id`, `created_at`, `updated_at`) VALUES
	(1, 'điều khiển thông minh', 'dieu-khien-thong-minh', 'App\\Model\\Solution', 1, '2020-06-03 13:23:05', '2020-06-03 13:23:05'),
	(2, 'cửa cuốn thông minh', 'cua-cuon-thong-minh', 'App\\Model\\Solution', 2, '2020-06-03 13:24:49', '2020-06-03 13:24:49'),
	(3, 'khách sạn thông minh', 'khach-san-thong-minh', 'App\\Model\\Solution', 3, '2020-06-03 13:27:30', '2020-06-03 13:27:30'),
	(4, 'an toàn & an ninh thông minh', 'an-toan-an-ninh-thong-minh', 'App\\Model\\Solution', 4, '2020-06-03 13:29:17', '2020-06-03 13:29:17'),
	(5, 'khóa thông minh', 'khoa-thong-minh', 'App\\Model\\Solution', 5, '2020-06-03 13:32:41', '2020-06-03 13:32:41'),
	(6, 'điều hòa thông minh', 'dieu-hoa-thong-minh', 'App\\Model\\Solution', 6, '2020-06-03 13:33:32', '2020-06-03 13:33:32'),
	(7, 'trang chủ', '/', 'App\\Model\\Menu', 1, '2020-06-03 13:56:23', '2020-06-03 13:57:05'),
	(8, 'giới thiệu', 'gioi-thieu', 'App\\Model\\Menu', 2, '2020-06-03 13:57:33', '2020-06-03 13:57:33'),
	(9, 'giải pháp', 'giai-phap', 'App\\Model\\Menu', 3, '2020-06-03 13:57:45', '2020-06-12 14:44:47'),
	(10, 'thiết bị', 'thiet-bi', 'App\\Model\\Menu', 4, '2020-06-03 13:57:57', '2020-06-14 12:27:15'),
	(11, 'tin tức', 'tin-tuc', 'App\\Model\\Menu', 5, '2020-06-03 13:58:06', '2020-06-03 13:58:06'),
	(12, 'liên hệ', 'lien-he', 'App\\Model\\Menu', 6, '2020-06-03 13:58:15', '2020-06-03 13:58:15'),
	(13, 'liên hệ', 'lien-he-1', 'App\\Model\\Article', 1, '2020-06-03 14:17:18', '2020-06-03 14:17:18'),
	(14, 'giới thiệu', 'gioi-thieu-1', 'App\\Model\\Article', 2, '2020-06-03 14:27:28', '2020-06-03 14:27:28'),
	(15, 'privacy policy', 'privacy-policy', 'App\\Model\\Menu', 7, '2020-06-03 14:32:46', '2020-06-03 14:32:46'),
	(16, 'privacy policy', 'privacy-policy-1', 'App\\Model\\Article', 3, '2020-06-03 14:34:04', '2020-06-03 14:34:04'),
	(17, 'công tắc thông minh', 'cong-tac-thong-minh', 'App\\Model\\Production', 1, '2020-06-12 14:15:30', '2020-06-12 14:15:30'),
	(18, 'cảm biến chuyển động đa năng', 'cam-bien-chuyen-dong-da-nang', 'App\\Model\\Production', 2, '2020-06-14 11:55:51', '2020-06-14 11:55:51'),
	(19, 'camera 4g năng lượng mặt trời', 'camera-4g-nang-luong-mat-troi', 'App\\Model\\Production', 3, '2020-06-14 11:57:33', '2020-06-14 11:57:33'),
	(20, 'camera giám sát không dây 4g', 'camera-giam-sat-khong-day-4g', 'App\\Model\\Production', 4, '2020-06-14 11:58:40', '2020-06-14 11:58:40'),
	(21, 'đèn trang trí năng lượng mặt trời', 'den-trang-tri-nang-luong-mat-troi', 'App\\Model\\Production', 5, '2020-06-14 12:03:02', '2020-06-14 12:03:02'),
	(22, 'đèn đường năng lượng mặt trời', 'den-duong-nang-luong-mat-troi', 'App\\Model\\Production', 6, '2020-06-14 12:03:46', '2020-06-14 12:03:46'),
	(23, 'điều khiển hồng ngoại thông minh', 'dieu-khien-hong-ngoai-thong-minh', 'App\\Model\\Production', 7, '2020-06-14 12:06:34', '2020-06-14 12:06:34'),
	(24, 'điều nhiệt thông minh', 'dieu-nhiet-thong-minh', 'App\\Model\\Production', 8, '2020-06-14 12:08:31', '2020-06-14 12:08:31'),
	(25, 'khóa thông minh', 'khoa-thong-minh-1', 'App\\Model\\Production', 9, '2020-06-14 12:12:11', '2020-06-14 12:12:11'),
	(26, 'fresh review of coming trends for summer ‘15', 'fresh-review-of-coming-trends-for-summer-15', 'App\\Model\\News', 1, '2020-06-14 12:40:21', '2020-06-14 12:40:21'),
	(27, 'kết quả tìm kiếm kết quả tìm kiếm trên web  tin tức 24h mới nhất, tin nhanh, tin nóng hà', 'ket-qua-tim-kiem-ket-qua-tim-kiem-tren-web-tin-tuc-24h-moi-nhat-tin-nhanh-tin-nong-ha', 'App\\Model\\News', 2, '2020-06-14 13:11:41', '2020-06-14 13:11:41'),
	(28, 'menu a', 'menu-a-541', 'App\\Model\\Menu', 8, '2020-06-30 14:11:23', '2020-06-30 14:11:23'),
	(29, 'menu b', 'menu-b-448', 'App\\Model\\Menu', 9, '2020-06-30 14:11:54', '2020-06-30 14:17:02');
/*!40000 ALTER TABLE `nodes` ENABLE KEYS */;

-- Dumping structure for table gico.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table gico.productions
CREATE TABLE IF NOT EXISTS `productions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `feature` longtext COLLATE utf8mb4_unicode_ci,
  `technical` longtext COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.productions: ~9 rows (approximately)
/*!40000 ALTER TABLE `productions` DISABLE KEYS */;
INSERT INTO `productions` (`id`, `name`, `title`, `description`, `feature`, `technical`, `content`, `image_path`, `sort`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'công tắc thông minh', '', '<p>Điều khiển bật tắt, hẹn giờ... các thiết bị trong nhà như đèn, quạt... từ xa thông qua smartphone</p>\r\n<p>Liên kết với các cảm biến trong giải pháp thông qua Internet để tự động bật đèn, còi... khi có cảnh báo</p>', '<p>• Điều khiển bật tắt, hẹn giờ... các thiết bị trong nhà như đèn, quạt... từ xa thông qua smartphone</p>\r\n                                            <p>• Liên kết với các cảm biến trong giải pháp thông qua Internet để tự động bật đèn, còi... khi có cảnh báo</p>\r\n                                            <p>• Hoạt động theo lịch đặt sẵn khi khách hàng đi vắng để tạo cảm giác có người ở nhà nhằm đánh lừa kẻ gian</p>', '<p>• Điện áp: 220VAC 50Hz</p>\r\n                                            <p>• Nhiệt độ hoạt động: tối đa 65°C</p>\r\n                                            <p>• Công suất tiêu hao: 300mW</p>\r\n                                            <p>• Công suất tải: 1000W mỗi kênh</p>\r\n                                            <p>• Chuẩn giao tiếp: WiFi 802.11b/g/n 2.4GHz, bảo mật WEP/WPA/WPA2</p>\r\n                                            <p>• Khoảng cách kết nối: 15m đến Access Point</p>\r\n                                            <p>• Mặt chạm: cảm ứng điện dung mặt kính</p>\r\n                                            <p>• Kích thước: 120mm x 68mm x 38mm, 86mm x 86mm x 34mm</p>', NULL, 'images/uploads/production/159197133055.png', 1, 1, '2020-06-12 14:15:30', '2020-06-12 14:31:43'),
	(2, 'cảm biến chuyển động đa năng', '', '<p>Giám sát và phát hiện người di chuyển trong khu vực cảm biến. Gửi cảnh báo về smartphone và thực hiện báo động tại chỗ theo cài đặt của khách hàng.</p><p>Kết nối với khóa cửa và các cảm biến khác như 1 Gateway an ninh GICO</p><p>Tích hợp 1 kênh điều khiển đèn tự động<br></p>', NULL, NULL, '<p>Giám sát và phát hiện người di chuyển trong khu vực cảm biến. Gửi cảnh báo về smartphone và thực hiện báo động tại chỗ theo cài đặt của khách hàng.</p><p>Kết nối với khóa cửa và các cảm biến khác như 1 Gateway an ninh GICO</p><p>Tích hợp 1 kênh điều khiển đèn tự động</p>', 'images/uploads/production/159213575192.png', 1, 1, '2020-06-14 11:55:51', '2020-06-14 11:55:51'),
	(3, 'camera 4g năng lượng mặt trời', '', '<p>Giải pháp camera 4g sử dụng năng lượng mặt trời được ứng dụng giám sát các khu vực như: công trường, nông trại, tàu thuyền đánh bắt hải sản, khu dân cư hoặc các villa</p><p>Giải pháp này giúp loại bỏ các yêu cầu về cơ sở hạ tầng, thêm vào đó các yêu cầu bảo trì và thêm vào đó khả năng hoạt động trong môi trường khắc nhiệt<br></p>', NULL, NULL, '<p>Giải pháp camera 4g sử dụng năng lượng mặt trời được ứng dụng giám sát các khu vực như: công trường, nông trại, tàu thuyền đánh bắt hải sản, khu dân cư hoặc các villa</p><p>Giải pháp này giúp loại bỏ các yêu cầu về cơ sở hạ tầng, thêm vào đó các yêu cầu bảo trì và thêm vào đó khả năng hoạt động trong môi trường khắc nhiệt</p>', 'images/uploads/production/159213585328.png', 1, 1, '2020-06-14 11:57:33', '2020-06-14 11:57:33'),
	(4, 'camera giám sát không dây 4g', '', '<p>Giải pháp IP Camera giám sát không dây 4G sử dụng cho nông trại, giao thông đường phố, sân vườn, cảnh báo đột nhập, quan sát công trình xây dựng trên cao... những nơi mà khó thi công hệ thống internet<br></p>', NULL, NULL, '<p>Giải pháp IP Camera giám sát không dây 4G sử dụng cho nông trại, giao thông đường phố, sân vườn, cảnh báo đột nhập, quan sát công trình xây dựng trên cao... những nơi mà khó thi công hệ thống internet<br></p>', 'images/uploads/production/15921359205.png', 1, 1, '2020-06-14 11:58:40', '2020-06-14 11:58:40'),
	(5, 'đèn trang trí năng lượng mặt trời', '', '<p>Không cần hệ thống dây điện phức tạp, phù hợp với các hoạt động ngoài trời, cảnh quan trang trí, đường đi, hồ bơi</p><p>Hoạt động chiếu sáng liên tục suốt 12 giờ khi sạc đầy cùng thời gian sạc khoảng 6 giờ<br></p>', NULL, NULL, '<p>Không cần hệ thống dây điện phức tạp, phù hợp với các hoạt động ngoài trời, cảnh quan trang trí, đường đi, hồ bơi</p><p>Hoạt động chiếu sáng liên tục suốt 12 giờ khi sạc đầy cùng thời gian sạc khoảng 6 giờ</p>', 'images/uploads/production/159213618291.png', 1, 1, '2020-06-14 12:03:02', '2020-06-14 12:03:02'),
	(6, 'đèn đường năng lượng mặt trời', '', '<p>Đèn đường năng lượng mặt trời có thiết kế lắp được trên các trụ điện hoặc tường rào với thời gian chiếu sáng liên tục đến 24 giờ trong ngày</p><p>Người sử dụng có thể lắp đặt thiết bị đèn ở trước cổng nhà, sân vườn, tường rào quanh nhà, nhà xưởng<br></p>', NULL, NULL, '<p>Đèn đường năng lượng mặt trời có thiết kế lắp được trên các trụ điện hoặc tường rào với thời gian chiếu sáng liên tục đến 24 giờ trong ngày</p><p>Người sử dụng có thể lắp đặt thiết bị đèn ở trước cổng nhà, sân vườn, tường rào quanh nhà, nhà xưởng</p>', 'images/uploads/production/159213622687.png', 1, 1, '2020-06-14 12:03:46', '2020-06-14 12:03:46'),
	(7, 'điều khiển hồng ngoại thông minh', '', '<p>Thiết bị cho phép điều khiển từ xa các thiết bị dân dụng trong gia đình như: điều hòa, TV, đầu thu kỹ thuật số, ampli... thông qua smartphone ở bất cứ nơi đâu chỉ cần có kết nối Internet</p><p>Kết hợp cùng các giải pháp khác để lên lịch bật tắt hay diều khiển nhiệt độ điều hòa theo thời gian<br></p>', NULL, NULL, '<p><br></p>', 'images/uploads/production/159213639456.png', 1, 1, '2020-06-14 12:06:34', '2020-06-14 12:06:34'),
	(8, 'điều nhiệt thông minh', '', '<p>Điều khiển máy lạnh có sử dụng bộ điều khiển từ xa dùng hồng ngoại cũng như hệ thống điều hòa trung tâm qua smartphone</p><p>Kết hợp cảm biến nhiệt độ, độ ẩm và cảm biến chuyển động để điều khiển máy lạnh tự động đem lại sự thoải mái<br></p>', NULL, NULL, NULL, 'images/uploads/production/159213651176.png', 1, 1, '2020-06-14 12:08:31', '2020-06-14 12:08:31'),
	(9, 'khóa thông minh', '', '<p>Khóa cửa điện tử tích hợp kết nối Bluetooth cho phép người dùng đóng/ mở/giám sát cửa từ xa thông qua smartphone, hoặc đóng/mở cửa bằng thẻ RFID, chìa khóa cơ hay bàn phím cảm ứng.</p><p>Hệ thống phân quyền tài khoản chủ và tài khoản khách. Cảnh báo khi có người can thiệp trái phép vào ổ khóa.</p>', NULL, NULL, '<p><br></p>', 'images/uploads/production/159343751182.jpg', 1, 1, '2020-06-14 12:12:11', '2020-06-29 13:31:51');
/*!40000 ALTER TABLE `productions` ENABLE KEYS */;

-- Dumping structure for table gico.solutions
CREATE TABLE IF NOT EXISTS `solutions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` tinyint(4) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.solutions: ~6 rows (approximately)
/*!40000 ALTER TABLE `solutions` DISABLE KEYS */;
INSERT INTO `solutions` (`id`, `name`, `title`, `description`, `content`, `image_path`, `icon_path`, `sort`, `active`, `created_at`, `updated_at`) VALUES
	(1, 'điều khiển thông minh', 'điều khiển', '<p>GICO cung cấp các công tắc, ổ cắm điện tử có kết nối internet theo công nghệ IoT giúp khách hàng điều khiển và giám sát các thiết bị điện trong nhà/căn hộ/offi ce thông qua smartphone.</p>\r\n<p>Người sử dụng có thể tự thiết lập những ngữ cảnh điều khiển hệ thống điện trong nhà/căn hộ/office thông qua smartphone hoặc điều khiển rèm cửa, mái cuốn... để tận dụng ánh sáng tự nhiên tiết kiệm năng lượng điện.</p>', '<p>GICO cung cấp các công tắc, ổ cắm điện tử có kết nối internet theo công nghệ IoT giúp khách hàng điều khiển và giám sát các thiết bị điện trong nhà/căn hộ/offi ce thông qua smartphone.</p>\r\n<p>Người sử dụng có thể tự thiết lập những ngữ cảnh điều khiển hệ thống điện trong nhà/căn hộ/office thông qua smartphone hoặc điều khiển rèm cửa, mái cuốn... để tận dụng ánh sáng tự nhiên tiết kiệm năng lượng điện.</p>', 'images/uploads/solution/159119058589.png', 'images/uploads/solution/159119058542.png', 1, 1, '2020-06-03 13:23:05', '2020-06-03 13:23:05'),
	(2, 'cửa cuốn thông minh', 'cửa cuốn', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Mở/đóng cửa mọi lúc mọi nơi với sự quan sát trực ঞ ếp từ camera trên cùng 1 app qua smartphone.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Phân quyền điều khiển cho phép chia sẻ cho nhiều người trong gia đình với các cấp điều khiển khác nhau. Lưu trữ lịch sử hoạt động. Tính năng mở cửa khẩn cấp.</span><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;"><br></span><br></p>', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Mở/đóng cửa mọi lúc mọi nơi với sự quan sát trực ঞ ếp từ camera trên cùng 1 app qua smartphone.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Phân quyền điều khiển cho phép chia sẻ cho nhiều người trong gia đình với các cấp điều khiển khác nhau. Lưu trữ lịch sử hoạt động. Tính năng mở cửa khẩn cấp.</span></p>', 'images/uploads/solution/159119068959.png', 'images/uploads/solution/159119068989.png', 1, 1, '2020-06-03 13:24:49', '2020-06-03 13:24:49'),
	(3, 'khách sạn thông minh', 'khách sạn', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Đây được xem là xu hướng công nghệ 4.0 của ngành quản lý khách sạn, giải pháp quản lý khách sạn thông minh của GICO kết hợp giải pháp nhà thông minh và dịch vụ liên quan giúp gia tăng giá trị cho người thuê phòng, tối ưu chi phí quản lý của người chủ khách sạn và kết nối chuỗi giá trị dịch vụ trong ngành quản lý khách sạn.</span><br></p>', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Đây được xem là xu hướng công nghệ 4.0 của ngành quản lý khách sạn, giải pháp quản lý khách sạn thông minh của GICO kết hợp giải pháp nhà thông minh và dịch vụ liên quan giúp gia tăng giá trị cho người thuê phòng, tối ưu chi phí quản lý của người chủ khách sạn và kết nối chuỗi giá trị dịch vụ trong ngành quản lý khách sạn.</span><br></p>', 'images/uploads/solution/159119085085.png', 'images/uploads/solution/159119085096.png', 1, 1, '2020-06-03 13:27:30', '2020-06-03 13:27:30'),
	(4, 'an toàn & an ninh thông minh', 'an toàn an ninh', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Bảo vệ ngôi nhà mọi lúc mọi nơi với hệ thống an ninh 3 lớp.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Gửi cảnh báo đến smartphone của khách hàng ngay khi phát hiện sự cố, thực hiện cảnh báo tại chỗ. Hỗ trợ cho chủ nhà: lịch sử cảnh báo, lưu trữ đám mây, xem hình ảnh báo động.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Tự động khóa cửa nhằm bảo vệ người trong nhà khi phát hiện có đối tượng lạ xâm nhập vào nhà.</span><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;"><br></span><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;"><br></span><br></p>', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Bảo vệ ngôi nhà mọi lúc mọi nơi với hệ thống an ninh 3 lớp.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Gửi cảnh báo đến smartphone của khách hàng ngay khi phát hiện sự cố, thực hiện cảnh báo tại chỗ. Hỗ trợ cho chủ nhà: lịch sử cảnh báo, lưu trữ đám mây, xem hình ảnh báo động.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; orphans: 1; text-align: justify; widows: 1;">Tự động khóa cửa nhằm bảo vệ người trong nhà khi phát hiện có đối tượng lạ xâm nhập vào nhà.</span></p>', 'images/uploads/solution/159119095748.png', 'images/uploads/solution/159119095797.png', 1, 1, '2020-06-03 13:29:17', '2020-06-03 13:29:17'),
	(5, 'khóa thông minh', 'khóa', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;">Khóa cửa điện tử tích hợp kết nối Bluetooth cho phép người dùng đóng/ mở/giám sát cửa từ xa thông qua smartphone, hoặc đóng/mở cửa bằng thẻ RFID, chìa khóa cơ hay bàn phím cảm ứng.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;">Hệ thống phân quyền tài khoản chủ và tài khoản khách. Cảnh báo khi có người can thiệp trái phép vào ổ khóa.</span><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;"><br></span><br></p>', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;">Khóa cửa điện tử tích hợp kết nối Bluetooth cho phép người dùng đóng/ mở/giám sát cửa từ xa thông qua smartphone, hoặc đóng/mở cửa bằng thẻ RFID, chìa khóa cơ hay bàn phím cảm ứng.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;">Hệ thống phân quyền tài khoản chủ và tài khoản khách. Cảnh báo khi có người can thiệp trái phép vào ổ khóa.</span></p>', 'images/uploads/solution/159119116153.png', 'images/uploads/solution/159119116190.png', 1, 1, '2020-06-03 13:32:41', '2020-06-14 13:37:38'),
	(6, 'điều hòa thông minh', 'điều hòa', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;">Điều khiển máy lạnh có sử dụng bộ điều khiển từ xa dùng hồng ngoại cũng như hệ thống điều hòa trung tâm qua smartphone. Kết hợp cảm biến nhiệt độ, độ ẩm và cảm biến chuyển động để điều khiển máy lạnh tự động đem lại sự thoải mái.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;">Thuật toán tối ưu giúp tiết kiệm điện năng tiêu thụ.</span><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;"><br></span><br></p>', '<p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;">Điều khiển máy lạnh có sử dụng bộ điều khiển từ xa dùng hồng ngoại cũng như hệ thống điều hòa trung tâm qua smartphone. Kết hợp cảm biến nhiệt độ, độ ẩm và cảm biến chuyển động để điều khiển máy lạnh tự động đem lại sự thoải mái.</span></p><p><span style="color: rgb(33, 37, 41); font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"; orphans: 1; text-align: justify; widows: 1;">Thuật toán tối ưu giúp tiết kiệm điện năng tiêu thụ.</span></p>', 'images/uploads/solution/159343810768.jpg', 'images/uploads/solution/159343812487.png', 1, 1, '2020-06-03 13:33:32', '2020-06-29 13:42:04');
/*!40000 ALTER TABLE `solutions` ENABLE KEYS */;

-- Dumping structure for table gico.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_account_unique` (`account`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table gico.users: ~2 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `account`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin', 'admin@gmail.com', NULL, '$2y$10$A2jKdd38a9WBdMA2rS823.KsVhadMkPpiPqeL0i.DewcljDvzivsK', NULL, '2020-05-23 13:22:26', '2020-06-22 13:53:46'),
	(2, 'hong', 'hong', 'hong@123', NULL, '$2y$10$xQYwqQxedE8FejFVmT5xYurckwwfHtFPV0JU6XewBUm7u6vyI7Rsu', NULL, '2020-05-23 14:01:37', '2020-05-23 14:01:37');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
