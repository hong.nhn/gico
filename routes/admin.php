<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Admin\DashboardController')->name('dashboard');

Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('change-password', 'Auth\ResetPasswordController@showForm')->name('password.change');
Route::post('change-password', 'Auth\ResetPasswordController@update');

Route::post('logout', 'Auth\LoginController@logout');

Route::group(['as' => 'admin.'], function(){
    Route::resource('app_text', 'Admin\AppTextController');

    Route::resource('solution', 'Admin\SolutionController');
    Route::put('solution/{solution}/update-active', 'Admin\SolutionController@updateActive')->name('solution.update.active');
    Route::put('solution/{solution}/update-sort', 'Admin\SolutionController@updateSort')->name('solution.update.sort');

    Route::resource('image-upload', 'Admin\ImageUploadController');

    Route::resource('menu', 'Admin\MenuController');
    Route::put('menu/{menu}/update-active', 'Admin\MenuController@updateActive')->name('menu.update.active');
    Route::put('menu/{menu}/update-sort', 'Admin\MenuController@updateSort')->name('menu.update.sort');
    Route::put('menu/{menu}/update-top', 'Admin\MenuController@updateShowTop')->name('menu.update.top');
    Route::put('menu/{menu}/update-footer', 'Admin\MenuController@updateShowFooter')->name('menu.update.footer');

    Route::resource('customer_rating', 'Admin\CustomerRatingController');
    Route::put('customer_rating/{customer_rating}/update-active', 'Admin\CustomerRatingController@updateActive')->name('customer_rating.update.active');
    Route::put('customer_rating/{customer_rating}/update-sort', 'Admin\CustomerRatingController@updateSort')->name('customer_rating.update.sort');

    Route::resource('article', 'Admin\ArticleController');
    Route::put('article/{article}/update-active', 'Admin\ArticleController@updateActive')->name('article.update.active');
    Route::put('article/{article}/update-menu', 'Admin\ArticleController@updateMenu')->name('article.update.menu');

    Route::resource('construction', 'Admin\ConstructionController');
    Route::put('construction/{construction}/update-active', 'Admin\ConstructionController@updateActive')->name('construction.update.active');
    Route::put('construction/{construction}/update-sort', 'Admin\ConstructionController@updateSort')->name('construction.update.sort');

    Route::resource('production', 'Admin\ProductionController');
    Route::put('production/{production}/update-active', 'Admin\ProductionController@updateActive')->name('production.update.active');
    Route::put('production/{production}/update-sort', 'Admin\ProductionController@updateSort')->name('production.update.sort');

    Route::resource('news', 'Admin\NewsController');
    Route::put('news/{news}/update-active', 'Admin\NewsController@updateActive')->name('news.update.active');
    Route::put('news/{news}/update-sort', 'Admin\NewsController@updateSort')->name('news.update.sort');
});

