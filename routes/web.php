<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Front\TopController')->name('index');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('{slug}', ['uses' => 'Front\PageController@index'])->where('slug', '([A-Za-z0-9\-\/]+)');

//Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::prefix('admin')->middleware('auth')->group(function () {
//     Route::get('/', function () {
//     return (1); die;
//     })->name('admin.amin');

//     Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//     Route::post('/register', 'Auth\RegisterController@register');
// });
