<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $menus = array();
        $menus['top'] = \App\Model\Menu::topList();
        $menus['top'] = isset($menus['top']) ? $menus['top'] : null;
        $menus['footer'] = \App\Model\Menu::footerList();
        $menus['footer'] = isset($menus['footer']) ? $menus['footer'] : null;
        //dd($menus);

        $generalText = array();
        $generalText['top'] = \App\Model\AppText::topList();
        $generalText['top'] = isset($generalText['top']) ? $generalText['top'] : null;
        $generalText['footer'] = \App\Model\AppText::footerList();
        $generalText['footer'] = isset($generalText['footer']) ? $generalText['footer'] : null;
        //dd($generalText);

        \View::share(['menus' => $menus, 'generalText' => $generalText]);
    }
}
