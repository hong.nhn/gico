<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    public function showForm()
    {
        return view('auth.passwords.reset');
    }

    public function update(Request $request)
    {
        try {
            $user = \App\User::findOrFail(Auth::id());

            if (Hash::check($request->password_old, $user->password)) {
                if ($request->password != $request->password_confirmation) {
                    return redirect()->back()
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Xác nhận mật khẩu mới không chính xác'
                    ]);
                }
                $user->update([
                    'password' => Hash::make($request->password)
                ]);

               return redirect()->route('dashboard')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Thay đổi Mật khẩu thành công'
                    ]);

            } else {
                return redirect()->back()
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Mật khẩu cũ không chính xác'
                    ]);
            }

        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }
}
