<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TopController extends Controller
{
    public function __invoke()
    {
    	$data = array();

    	$constructions = \App\Model\Construction::frontList();
    	$solutions = \App\Model\Solution::frontList();
    	$productions = \App\Model\Production::topPageList();
    	$customerRating = \App\Model\CustomerRating::frontList();
        $generalText = \App\Model\AppText::general(TEXT_TOP_PAGE);

    	$data = [
    		'constructions' => $constructions,
    		'solutions' => $solutions,
    		'productions' => $productions,
    		'customerRating' => $customerRating,
            'generalText' => $generalText
    	];

    	//dd($data);

    	return view('front.pages.index', compact('data'));
    }
}
