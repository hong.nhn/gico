<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index($slug = null)
	{
	    $page = \App\Model\Node::where('slug', $slug);

	    $page = $page->firstOrFail();
        //dd($page);

	    if ($page->nodeable_type == MODEL_MENU)
	    {
	    	return $this->getMenu($page);
	    }
	    else if ($page->nodeable_type == MODEL_SOLUTION)
	    {
	    	return $this->showSolution($page);
	    }
        else if ($page->nodeable_type == MODEL_PRODUCTION)
        {
            return $this->showProduction($page);
        }
        else if ($page->nodeable_type == MODEL_NEWS)
        {
            return $this->showNews($page);
        }

	    abort(404);
	}

	public function getMenu($page)
	{
		//dd($page);
		if ($page->nodeable->type == TYPE_SOLUTION)
			return $this->showSolutionList($page->name);
		elseif ($page->nodeable->type == TYPE_PRODUCTION)
			return $this->showProductionList($page->name);
        elseif ($page->nodeable->type == TYPE_NEW)
            return $this->showNewsList($page->name);
		elseif ($page->nodeable->type == TYPE_ARTICLE)
			return $this->showArticle($page);
		else
			abort(404);
	}

	public function showSolutionList($title)
	{
        $data = array();

		$list = \App\Model\Solution::frontList();

        $data = ['title' => $title, 'list' => $list];
        //dd($data);

		return view('front.pages.solution.index', compact('data'));
	}

	public function showProductionList($title)
	{
        $data = array();

		$list = \App\Model\Production::frontList();

        $data = ['title' => $title, 'list' => $list];

		return view('front.pages.production.index', compact('data'));
	}

    public function showNewsList($title)
    {
        $data = array();

        $list = \App\Model\News::frontList();
        $updatedList = \App\Model\News::frontUpdateList();

        $data = ['title' => $title, 'list' => $list, 'updatedList' => $updatedList];

        return view('front.pages.news.index', compact('data'));
    }

	public function showArticle($page)
	{
        //dd(1);
        //dd($page->nodeable_id);
		$menu = \App\Model\Menu::findOrFail($page->nodeable_id);
        //dd($menu);
		$data = \App\Model\Article::findOrFail($menu->ref_id);
        //dd($data);

        if (isset($data->template) && !is_null($data->template)) {
            //dd(1);
            return view('front.pages.templates.' . TEMPLATE_NAME[$data->template], compact('data'));
        }
        //dd(2);
		return view('front.pages.article.show', compact('data'));
	}

	public function showSolution($page)
	{
		$data = \App\Model\Solution::findOrFail($page->nodeable_id);

		return view('front.pages.solution.show', compact('data'));
	}

    public function showProduction($page)
    {
        $data = \App\Model\Production::findOrFail($page->nodeable_id);
        //dd($data);

        return view('front.pages.production.show', compact('data'));
    }

    public function showNews($page)
    {
        $data = \App\Model\News::findOrFail($page->nodeable_id);
        //dd($data);

        return view('front.pages.news.show', compact('data'));
    }
}
