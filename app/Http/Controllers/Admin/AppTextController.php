<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\AppText;

class AppTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = AppText::active();

        return view(PATH_APP_TEXT.'index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(PATH_APP_TEXT.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = AppText::create($request->all());

            if ((boolean)$data == true) {
                return redirect()->route('admin.app_text.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Tạo mới thành công'
                    ]);
            } else {
                return redirect()->route('admin.app_text.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = AppText::findOrFail($id);

        return view(PATH_APP_TEXT.'edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = AppText::findOrFail($id);
            $data->update($request->all());

            if ((boolean)$data == true) {
                return redirect()->route('admin.app_text.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Cập nhật thành công'
                    ]);
            } else {
                return redirect()->route('admin.app_text.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
