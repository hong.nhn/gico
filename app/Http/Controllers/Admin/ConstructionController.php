<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Construction;

class ConstructionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Construction::sort();

        return view(PATH_CONSTRUCTION.'index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view (PATH_CONSTRUCTION.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $image_path = $this->uploadImage($request->file('image_file'));
            $data = Construction::create([
                'name'        => $request->name,
                'image_path'  => $image_path
            ]);

            if ((boolean)$data == true) {
                return redirect()->route('admin.construction.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Tạo mới thành công'
                    ]);
            } else {
                return redirect()->route('admin.construction.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    public function uploadImage($image)
    {
        $imagePath = $image;
        $imageName = time() . rand(1,100) . '.' . $imagePath->getClientOriginalExtension();

        $status = $image->move(public_path(PATH_CONSTRUCTION_IMAGE), $imageName);
        if ((boolean)$status)
            return PATH_CONSTRUCTION_IMAGE . '/' . $imageName;
        else
            return PATH_DEFAULT_IMAGE;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Construction::findOrFail($id);

        return view(PATH_CONSTRUCTION.'edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = Construction::findOrFail($id)
                ->update($request->all());

            if ($request->hasFile('image_file')) {
                $image_path = $this->uploadImage($request->file('image_file'));
                $data = Construction::findOrFail($id)->update([
                    'image_path'  => $image_path
                ]);
            }

            if ((boolean)$data == true) {
                return redirect()->route('admin.construction.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Cập nhật thành công'
                    ]);
            } else {
                return redirect()->route('admin.construction.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }
    public function updateActive(Request $request, $id)
    {
        try {
            $active = (isset($request->active) ? config('desf.admin.active') : config('desf.admin.inactive'));

            $data = Construction::findOrFail($id)->update([
                'active' => $active
            ]);

            if ((boolean)$data) {
                return redirect()->route('admin.construction.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Cập nhật thành công'
                    ]);
            } else {
                return redirect()->route('admin.construction.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    public function updateSort(Request $request, $id)
    {
        try {
            if ((int)$request->sort < 1 || (int)$request->sort > 40) {
                return redirect()->route('admin.construction.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
                exit();
            }

            $data = Construction::findOrFail($id)->update([
                'sort' => $request->sort
            ]);

            if ((boolean)$data) {
                return redirect()->route('admin.construction.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Cập nhật thành công'
                    ]);
            } else {
                return redirect()->route('admin.construction.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
