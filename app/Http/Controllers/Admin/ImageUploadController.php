<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ImageUpload;

class ImageUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = ImageUpload::sort();
        return view(PATH_IMAGE_UPLOAD.'index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view(PATH_IMAGE_UPLOAD.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try {
            $image_path = $this->uploadImage($request->file('image_file'));

            $image = ImageUpload::create([
                'image_path'  => $image_path
            ]);

            if ((boolean)$image) {
                return redirect()->route('admin.image-upload.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Thêm thành công'
                    ]);
            } else {
                return redirect()->route('admin.image-upload.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        // } catch (\Exception $e) {
        //     return redirect()->route('dashboard')
        //         ->with([
        //             'f_level' => 'error',
        //             'f_message' => 'Catch Error'
        //         ]);
        // }
    }

    public function uploadImage($image)
    {
        $imagePath = $image;
        $imageName = time() . '.' . $imagePath->getClientOriginalExtension();

        $status = $image->move(public_path(PATH_IMAGE_UPLOAD_IMAGE), $imageName);
        if ((boolean)$status)
            return PATH_IMAGE_UPLOAD_IMAGE . '/' . $imageName;
        else
            return PATH_DEFAULT_IMAGE;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
