<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Menu;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Menu::sort();

        return view(PATH_MENU.'index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view (PATH_MENU.'create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $menu = Menu::create($request->all());

            $node = $menu->node()->create(['name' => $request->name]);

            if ((boolean)$node->save()) {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Tạo mới thành công'
                    ]);
            } else {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Menu::findOrFail($id);

        return view(PATH_MENU.'edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = Menu::findOrFail($id)->update([
                'name' => $request->name,
                'type' => $request->type
            ]);

            $data_node = Menu::findOrFail($id);

            $node = $data_node->node()->update(['slug' => $request->slug]);

            if ((boolean)$node) {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Cập nhật thành công'
                    ]);
            } else {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }
    public function updateActive(Request $request, $id)
    {
        try {
            $active = (isset($request->active) ? config('desf.admin.active') : config('desf.admin.inactive'));

            $data = Menu::findOrFail($id)->update([
                'active' => $active
            ]);

            if ((boolean)$data) {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Cập nhật thành công'
                    ]);
            } else {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    public function updateShowTop(Request $request, $id)
    {
        try {
            $showTop = (isset($request->top) ? config('desf.admin.active') : config('desf.admin.inactive'));

            $data = Menu::findOrFail($id)->update([
                'show_top' => $showTop
            ]);

            if ((boolean)$data) {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Cập nhật thành công'
                    ]);
            } else {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    public function updateShowFooter(Request $request, $id)
    {
        try {
            $showFooter = (isset($request->footer) ? config('desf.admin.active') : config('desf.admin.inactive'));

            $data = Menu::findOrFail($id)->update([
                'show_footer' => $showFooter
            ]);

            if ((boolean)$data) {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Cập nhật thành công'
                    ]);
            } else {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    public function updateSort(Request $request, $id)
    {
        try {
            if ((int)$request->sort < 1 || (int)$request->sort > 40) {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
                exit();
            }

            $data = Menu::findOrFail($id)->update([
                'sort' => $request->sort
            ]);

            //dd($data);

            if ((boolean)$data) {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'success',
                        'f_message' => 'Cập nhật thành công'
                    ]);
            } else {
                return redirect()->route('admin.menu.index')
                    ->with([
                        'f_level' => 'error',
                        'f_message' => 'Đã xảy ra lỗi'
                    ]);
            }
        } catch (\Exception $e) {
            return redirect()->route('dashboard')
                ->with([
                    'f_level' => 'error',
                    'f_message' => 'Catch Error'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
