<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Construction extends Model
{
    protected $fillable = ['name', 'image_path', 'sort', 'active'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->name = Str::lower(trim($model->name));
        });

        static::updating(function ($model) {
            $model->name = Str::lower(trim($model->name));
        });
    }

    public function scopeSort($query)
    {
        return $query->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }
    public function getActiveTitleAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_title');
        else
            return config('desf.admin.inactive_title');
    }

    public function getActiveClassAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_class');
        else
            return config('desf.admin.inactive_class');
    }

    public function scopeFrontList($query)
    {
        return $query->where('active', ACTIVE)
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }
}
