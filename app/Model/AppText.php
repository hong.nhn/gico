<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class AppText extends Model
{
    protected $fillable = ['name', 'link', 'image_path', 'type', 'active'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->name = Str::lower(trim($model->name));
        });

        static::updating(function ($model) {
            $model->name = Str::lower(trim($model->name));
        });
    }

    public function scopeActive($query)
    {
        return $query->where('active', ACTIVE)
            ->get();
    }

    public function scopeGeneral($filter, $type)
    {
        return $filter->where(function($query) use ($type) {
                $query->where('active', ACTIVE)
                    ->where('type', $type);
        })->get();
    }

    public function scopeTopList($query)
    {
        return $query->where('active', ACTIVE)
            ->where('type', TEXT_HEADER)
            ->orderBy('id', 'ASC')
            ->get();
    }

    public function scopeFooterList($query)
    {
        return $query->where('active', ACTIVE)
            ->where('type', TEXT_FOOTER)
            ->orderBy('id', 'ASC')
            ->get();
    }
}
