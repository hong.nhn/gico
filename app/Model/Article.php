<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Article extends Model
{
    protected $fillable = ['name', 'title', 'description', 'content', 'image_path', 'template', 'active'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->name = Str::lower(trim($model->name));
            $model->title = Str::lower(trim($model->title));
        });

        static::updating(function ($model) {
            $model->name = Str::lower(trim($model->name));
            $model->title = Str::lower(trim($model->title));
        });
    }
    /**
     * Get the Solution's Node.
     */
    public function node()
    {
        return $this->morphOne('App\Model\Node', 'nodeable');
    }

    public function scopeSort($query)
    {
        return $query->orderBy('id', 'DESC')
            ->get();
    }

    public function scopeActive($query)
    {
        return $query->where('active', ACTIVE)
            ->get();
    }

    public function getActiveTitleAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_title');
        else
            return config('desf.admin.inactive_title');
    }

    public function getActiveClassAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_class');
        else
            return config('desf.admin.inactive_class');
    }
    public function scopeFrontList($query)
    {
        return $query->where('active', ACTIVE)
            ->orderBy('id', 'DESC')
            ->get();
    }
}
