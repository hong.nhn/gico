<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Node extends Model
{
    protected $fillable = ['name', 'slug', 'nodeable_type', 'nodeable_id'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->name = Str::lower(trim($model->name));
            // produce a slug based on the activity title
            $slug = \Str::slug(trim($model->name));

            // check to see if any other slugs exist that are the same & count them
            $count = rand(1,1000);

            // if other slugs exist that are the same, append the count to the slug
            $model->slug = $count ? "{$slug}-{$count}" : $slug;
            });

        static::updating(function ($model) {
            $model->name = Str::lower(trim($model->name));
        });
    }
    /**
     * Get all of the owning nodeable models.
     */
    public function nodeable()
    {
        return $this->morphTo();
    }
}
