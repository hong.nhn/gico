<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImageUpload extends Model
{
	protected $fillable = ['image_path'];

    public function scopeSort($query)
    {
        return $query->orderBy('id', 'DESC')->get();
    }

    public function getImagePathFullAttribute()
    {
    	return url('/') . '/' . $this->image_path;
    }
}
