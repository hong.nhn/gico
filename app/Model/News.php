<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class News extends Model
{
    protected $fillable = ['name', 'description', 'content', 'image_path', 'sort', 'active'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->name = Str::lower(trim($model->name));
        });

        static::updating(function ($model) {
            $model->name = Str::lower(trim($model->name));
        });
    }
    /**
     * Get the Solution's Node.
     */
    public function node()
    {
        return $this->morphOne('App\Model\Node', 'nodeable');
    }

    public function scopeSort($query)
    {
        return $query->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function scopeActive($query)
    {
        return $query->where('active', ACTIVE)
            ->get();
    }

    public function getTime()
    {
        return Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y');
    }

    public function getActiveTitleAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_title');
        else
            return config('desf.admin.inactive_title');
    }

    public function getActiveClassAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_class');
        else
            return config('desf.admin.inactive_class');
    }

    public function scopeFrontList($query)
    {
        return $query->where('active', ACTIVE)
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function scopeFrontUpdateList($query)
    {
        return $query->where('active', ACTIVE)
            ->orderBy('id', 'DESC')
            ->take(3)
            ->get();
    }
}
