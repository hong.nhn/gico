<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Menu extends Model
{
    protected $fillable = ['name', 'type', 'sort', 'active', 'ref_id', 'show_top', 'show_footer'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->name = Str::lower(trim($model->name));
        });

        static::updating(function ($model) {
            $model->name = Str::lower(trim($model->name));
        });
    }
    /**
     * Get the Menu's Node.
     */
    public function node()
    {
        return $this->morphOne('App\Model\Node', 'nodeable');
    }

    public function scopeSort($query)
    {
        return $query->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function getActiveTitleAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_title');
        else
            return config('desf.admin.inactive_title');
    }

    public function getActiveClassAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_class');
        else
            return config('desf.admin.inactive_class');
    }

    public function scopeFrontList($query)
    {
        return $query->where('active', ACTIVE)
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function scopeTopList($query)
    {
        return $query->where('active', ACTIVE)
            ->where('show_top', ACTIVE)
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function scopeFooterList($query)
    {
        return $query->where('active', ACTIVE)
            ->where('show_footer', ACTIVE)
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }

    public function scopeArticles($query)
    {
        return $query->where('type', TYPE_ARTICLE)
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }
}
