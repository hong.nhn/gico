<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerRating extends Model
{
    protected $fillable = ['name', 'sort', 'star', 'content', 'active'];

    public function scopeSort($query)
    {
        return $query->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }
    public function getActiveTitleAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_title');
        else
            return config('desf.admin.inactive_title');
    }

    public function getActiveClassAttribute()
    {
        if ($this->active == config('desf.admin.active'))
            return config('desf.admin.active_class');
        else
            return config('desf.admin.inactive_class');
    }

    public function scopeFrontList($query)
    {
        return $query->where('active', ACTIVE)
            ->orderBy('sort', 'ASC')
            ->orderBy('id', 'DESC')
            ->get();
    }
}
