<?php
    const PATH_MENU = 'admin.pages.menu.';
    const PATH_APP_TEXT = 'admin.pages.app_text.';
    const PATH_IMAGE_UPLOAD = 'admin.pages.image_upload.';
    const PATH_SOLUTION = 'admin.pages.solution.';
    const PATH_CUSTOMER_RATING = 'admin.pages.customer_rating.';
    const PATH_ARTICLE = 'admin.pages.article.';
    const PATH_CONSTRUCTION = 'admin.pages.construction.';
    const PATH_PRODUCTION = 'admin.pages.production.';
    const PATH_NEWS = 'admin.pages.news.';

    const PATH_DEFAULT_IMAGE = 'images/default.jpg';
    const PATH_IMAGE_UPLOAD_IMAGE = 'images/uploads';
    const PATH_SOLUTION_IMAGE = 'images/uploads/solution';
    const PATH_CONSTRUCTION_IMAGE = 'images/uploads/construction';
    const PATH_PRODUCTION_IMAGE = 'images/uploads/production';
    const PATH_NEWS_IMAGE = 'images/uploads/news';

    const TYPE_SOLUTION = 1;
    const TYPE_PRODUCTION = 2;
    const TYPE_NEW = 3;
    const TYPE_ARTICLE = 4;
    const TYPE_TITLE = [
        TYPE_SOLUTION => 'GIẢI PHÁP',
        TYPE_PRODUCTION => 'SẢN PHẨM',
        TYPE_NEW => 'TIN TỨC',
        TYPE_ARTICLE => 'BẢI VIẾT'
    ];

    const TEXT_HEADER = 1;
    const TEXT_FOOTER = 2;
    const TEXT_TOP_PAGE = 3;
    const TEXT_TITLE = [
        TEXT_HEADER => 'HEADER',
        TEXT_FOOTER => 'FOOTER',
        TEXT_TOP_PAGE => 'TRANG CHỦ',
    ];

    const TEMPLATE_CONTACT = 1;
    const TEMPLATE_TITLE = [
        TEMPLATE_CONTACT => 'LIÊN HỆ'
    ];
    const TEMPLATE_NAME = [
        TEMPLATE_CONTACT => 'contact'
    ];

    const MODEL_MENU = 'App\Model\Menu';
    const MODEL_SOLUTION = 'App\Model\Solution';
    const MODEL_PRODUCTION = 'App\Model\Production';
    const MODEL_NEWS = 'App\Model\News';

    const ACTIVE = 1;

    const NEWS_UPDATED_CREATED_AT_TITLE = 'Đã đăng lúc';

    return [
        'admin' => [
            'active' => 1,
            'inactive' => 0,
            'active_title' => 'Active',
            'inactive_title' => 'In Active',
            'active_class' => 'success',
            'inactive_class' => 'warning',
            'pagination' => 15,
        ]
    ];
