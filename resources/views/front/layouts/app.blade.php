<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @stack('css')
    <title>GICO TECH</title>
</head>

<body>
    <header class="gico-header bg-white">
        <div class="header--top py-3 blue-bg d-none d-md-block">
            <div class="container">
                @include('front.blocks.navbar-top')
            </div>
        </div>
        <div class="container">
            <nav class="navbar navbar-expand-lg">
                @include('front.blocks.navbar-menu')
            </nav>
        </div>
    </header>
    @yield('banner')
    @yield('content')
    @include('front.blocks.footer')
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('js/jquery-3.5.1.slim.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    @stack('scripts')

</body>

</html>
