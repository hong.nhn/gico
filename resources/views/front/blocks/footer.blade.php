<footer>
    <div class="footer--wrapper">
        <div class="menu--wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-6 offset-3 offset-md-0">
                        <div class="logo">
                            @if (isset($generalText['footer'][0]))
                            <a href="{{ $generalText['footer'][0]->link }}"><img src="{{ asset($generalText['footer'][0]->image_path) }}" class="img-fluid"></a>
                            @endif
                        </div>
                        {{-- <div class="social">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <a href="#"><img src="images/icon/fb-white.png" class="img-fluid"></a>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="col-md-6">
                        <div class="navbar--bottom">
                            <div class="row">
                                @foreach ($menus['footer'] as $item)
                                <div class="col-md-6 col-6">
                                    <p class="text-capitalize">
                                        <a href="{{ $item->node->slug }}">{{ $item->name }}</a>
                                    </p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="contact--bottom">
                            <p>
                                @if (isset($generalText['footer'][1]))
                                <a href="{{ $generalText['footer'][1]->link }}">
                                    <img src="{{ asset($generalText['footer'][1]->image_path) }}" class="contact--icon">
                                    {{ $generalText['footer'][1]->name }}
                                </a>
                                @endif
                            </p>
                            <p>
                                @if (isset($generalText['footer'][2]))
                                <a href="{{ $generalText['footer'][2]->link }}">
                                    <img src="{{ asset($generalText['footer'][2]->image_path) }}" class="contact--icon">
                                    {{ $generalText['footer'][2]->name }}
                                </a>
                                @endif
                            </p>
                            <p>
                                @if (isset($generalText['footer'][3]))
                                <a href="{{ $generalText['footer'][3]->link }}">
                                    <img src="{{ asset($generalText['footer'][3]->image_path) }}" class="contact--icon">
                                    {{ $generalText['footer'][3]->name }}
                                </a>
                                @endif
                            </p>
                            <p>
                                @if (isset($generalText['footer'][4]))
                                <a href="{{ $generalText['footer'][4]->link }}">
                                    <img src="{{ asset($generalText['footer'][4]->image_path) }}" class="contact--icon">
                                    {{ $generalText['footer'][4]->name }}
                                </a>
                                @endif
                            </p>
                            <p class="text-capitalize">
                                @if (isset($generalText['footer'][5]))
                                <img src="{{ asset($generalText['footer'][5]->image_path) }}" class="contact--icon">
                                {{ $generalText['footer'][5]->name }}
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (isset($generalText['footer'][6]))
        <div class="copyright text-center text-capitalize">
            {{ $generalText['footer'][6]->name }}
        </div>
         @endif
    </div>
</footer>
