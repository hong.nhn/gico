<div class="row">
    <div class="col-md-3">
        <div class="row">
            @if (isset($generalText['top'][0]))
            <div class="col">
                <a href="{{ $generalText['top'][0]->link }}" class="text-white">{!! $generalText['top'][0]->image_path !!}</a>
            </div>
            @endif
            @if (isset($generalText['top'][1]))
            <div class="col">
                <a href="{{ $generalText['top'][1]->link }}" class="text-white">{!! $generalText['top'][1]->image_path !!}</a>
            </div>
            @endif
            @if (isset($generalText['top'][2]))
            <div class="col">
                <a href="{{ $generalText['top'][2]->link }}" class="text-white">{!! $generalText['top'][2]->image_path !!}</a>
            </div>
            @endif
        </div>
    </div>
    <div class="col-md-9 text-right">
        <ul class="list-inline mb-0">
            @if (isset($generalText['top'][3]))
            <li class="list-inline-item">
                <a href="{{ $generalText['top'][3]->link }}" class="btn text-white">
                    {!! $generalText['top'][3]->image_path !!}
                    {{ $generalText['top'][3]->name }}
                </a>
            </li>
            @endif
            {{-- <li class="list-inline-item">
                <button class="btn">
                    <i class="fa fa-search text-white" aria-hidden="true"></i>
                </button>
            </li> --}}
        </ul>
    </div>
</div>
