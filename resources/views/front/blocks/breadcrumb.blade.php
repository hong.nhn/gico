<section class="__breadcrumb">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-white pl-0 text-capitalize">
                    <li class="breadcrumb-item"><a href="/" class="gico-color">Trang chủ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">@isset($data['title']) {{ $data['title'] }} @endisset @empty($data['title']) {{ $data->name }} @endempty</li>
                </ol>
            </nav>
        </div>
    </section>
