@if (isset($generalText['top'][4]))
<a class="navbar-brand" href="{{ $generalText['top'][4]->link }}">
    <img src="{{ asset($generalText['top'][4]->image_path) }}" class="img-fluid" width="70" loading="lazy">
</a>
@endif
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav ml-auto">
        @foreach ($menus['top'] as $item)
        <a class="gico-nav-link nav-item nav-link gico-color text-capitalize" href="{{ $item->node->slug }}">{{ $item->name }}</a>
        @endforeach
    </div>
</div>
