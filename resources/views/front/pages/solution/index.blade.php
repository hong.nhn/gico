@extends('front.layouts.app')

@section('content')
<section class="banner-wrapper" style="position: relative;">
            <img src="front/images/banner/category.jpg" class="img-fluid" alt="">
            <div class="heading--sub carousel-caption text-capitalize d-none d-md-block">
                {{ $data['title'] }}
            </div>
        </section>
@include('front.blocks.breadcrumb')
<main role="main" class="solution-page">

        <section class="page--content--wrapper">
            <div class="container card-columns">
            	@foreach ($data['list'] as $item)
                <div class="card border-0 item--wrapper">
                    <a href="{{ $item->node->slug }}">
                        <img src="{{ asset($item->image_path) }}" class="item--image img-circle mx-auto d-block shadow rounded-circle img-fluid">
                    </a>
                    <div class="card-body shadow border border-secondary item--content text-justify">
                        <div class="item--title">
                            <a href="{{ $item->node->slug }}" class="gico-color text-uppercase">
                                {{ $item->name }}
                            </a>
                        </div>
                        {!! $item->description !!}
                    </div>
                </div>
                @endforeach
            </div>
        </section>
    </main>

@endsection
