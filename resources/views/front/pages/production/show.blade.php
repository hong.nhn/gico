@extends('front.layouts.app')
@section('content')
<section class="banner-wrapper" style="position: relative;">
    <img src="front/images/banner/category.jpg" class="img-fluid" alt="">
    <div class="heading--sub carousel-caption text-capitalize d-none d-md-block">
        {{ $data->name }}
    </div>
</section>
@include('front.blocks.breadcrumb')
<main role="main" class="production-child-page">
    <section class="page--content--wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="image--side">
                        <img src="{{ asset($data->image_path) }}" class="w-100">
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="content--side">
                        <div class="title text-uppercase">
                            {{ $data->name }}
                        </div>
                        <div id="accordion" class="product--accordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed text-uppercase" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Tính năng
                                            <i class="fas fa-angle-up"></i>
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        {!! $data->feature !!}
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="d-flex align-items-center justify-content-between btn btn-link collapsed text-uppercase" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Thông số kỹ thuật
                                            <i class="fas fa-angle-down"></i>
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        {!! $data->technical !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@push('scripts')
<script>
    $("#accordion").on("hide.bs.collapse show.bs.collapse", e => {
      $(e.target)
        .prev()
        .find("i:last-child")
        .toggleClass("fa-angle-up fa-angle-down");
    });
</script>
@endpush
