@extends('front.layouts.app')

@section('content')
<section class="banner-wrapper" style="position: relative;">
            <img src="front/images/banner/category.jpg" class="img-fluid" alt="">
            <div class="heading--sub carousel-caption text-capitalize d-none d-md-block">
                {{ $data['title'] }}
            </div>
        </section>
@include('front.blocks.breadcrumb')
<main role="main" class="production-page">
    <section class="page--content--wrapper">
            <div class="container">
                <div class="row">
                    @foreach ($data['list'] as $item)
                    <div class="col-md-6 mb-4">
                        <div class="card h-100 shadow-sm gico-border">
                            <div class="row no-gutters">
                                <div class="col-md-4">
                                    <div class="d-block d-sm-none">
                                        <a href="{{ $item->node->slug }}"><img src="{{ asset($item->image_path) }}" class="img-fluid d-block mx-auto"></a>
                                    </div>
                                    <div class="item--image d-none d-sm-block" style="background-image: url({{ asset($item->image_path) }});" >
                                        <a href="{{ $item->node->slug }}"></a>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="card-body item--desc">
                                        <div class="item--title text-capitalize">
                                            <a href="{{ $item->node->slug }}" class="gico-color">{{ $item->name }}</a>
                                        </div>
                                        {!! $item->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
</main>

@endsection
