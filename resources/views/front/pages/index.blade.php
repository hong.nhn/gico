@extends('front.layouts.app')

@push('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css">
@endpush

@section('banner')
<img src="{{ asset(isset($data['generalText'][4]->image_path) ? $data['generalText'][4]->image_path : null) }}" class="img-fluid" alt="">
@endsection

@section('content')
    <main role="main" class="top-page">
        @if (isset($data['constructions']) && !is_null($data['constructions']) && count($data['constructions']) > 0)
        <section class="construct wrapper">
            <div class="construct--wrapper">
                <div class="container">
                    <div class="heading text-center">
                        {!! isset($data['generalText'][0]->name) ? $data['generalText'][0]->name : null !!}
                        {{-- CÔNG TRÌNH TIÊU BIỂU --}}
                    </div>
                    <div class="content--wrapper">
                        <div class="row">
                            @foreach ($data['constructions'] as $item)
                            <div class="col-md-3">
                                <div class="item--wrapper">
                                    <div class="item--image">
                                        <img src="{{ asset($item->image_path) }}" class="img-fluid d-block mx-auto">
                                    </div>
                                    <div class="item--title">
                                        {{ $item->name }}
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif
        @if (isset($data['solutions']) && !is_null($data['solutions']) && count($data['solutions']) > 0)
        <section class="solution wrapper" style="background-image: url({{ asset(isset($data['generalText'][5]->image_path) ? $data['generalText'][5]->image_path : null) }});">
            <div class="solution--wrapper">
                <div class="container">
                    <div class="heading text-center">
                        {!! isset($data['generalText'][1]->name) ? $data['generalText'][1]->name : null !!}
                        {{-- GIẢI PHÁPIOT --}}
                    </div>
                    <div class="content--wrapper">
                        <div class="row">
                            <div class="col-md-10 offset-md-1">
                                <div class="row">
                                    @foreach ($data['solutions'] as $item)
                                    <div class="col-md-2 col-6">
                                        <div class="item--wrapper">
                                            <div class="icon text-center">
                                                <a href="{{ $item->node->slug }}"><img src="{{ asset($item->icon_path) }}" class="img-fluid"></a>
                                            </div>
                                            <div class="item--title text-center">
                                                <a href="{{ $item->node->slug }}" class="text-white">{{ $item->title }}</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif
        @if (isset($data['productions']) && !is_null($data['productions']) && count($data['productions']) > 0)
        <section class="product wrapper">
            <div class="product--wrapper">
                <div class="container">
                    <div class="heading text-center">
                        {!! isset($data['generalText'][2]->name) ? $data['generalText'][2]->name : null !!}
                        {{-- SẢN PHẨM --}}
                    </div>
                    <div class="content--wrapper">
                        <div class="row">
                            @foreach ($data['productions'] as $item)
                            <div class="col-md-4 mb-3">
                                <div class="card">
                                    <a href="{{ $item->node->slug }}" class="item--image"><img src="{{ asset($item->image_path) }}" class="card-img-top"></a>
                                    <div class="card-body">
                                        <div class="item--title"><a href="{{ $item->node->slug }}">{{ $item->name }}</a></div>
                                        <div class="desc">
                                            {!! $item->description !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
         @endif
         @if (isset($data['customerRating']) && !is_null($data['customerRating']) && count($data['customerRating']) > 0)
        <section class="custommer wrapper" style="background-image: url({{ asset(isset($data['generalText'][6]->image_path) ? $data['generalText'][6]->image_path : null) }});">
            <div class="custommer--wrapper">
                <div class="container">
                    <div class="heading text-center">
                        {!! isset($data['generalText'][3]->name) ? $data['generalText'][3]->name : null !!}
                        {{-- ĐÁNH GIÁ TỪ KHÁCH HÀNG --}}
                    </div>
                    <div class="content--wrapper">
                        <div id="custommer--slick">
                            @foreach ($data['customerRating'] as $item)
                            <div class="p-3">
                                <div class="card shadow-sm">
                                    <div class="card-header bg-white text-center border-bottom-0">
                                        <img src="{{ asset('front/images/icon/5-stars.png') }}" class="img-fluid d-block mx-auto">
                                    </div>
                                    <div class="card-body text-center">
                                        {!! $item->content !!}
                                        <strong>{{ $item->name }}</strong>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endif
    </main>

@endsection

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script>
    $('#custommer--slick').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        prevArrow:"<button type='button' class='custommer_slick_arrows slick-arrow slick-prev'><img src='images/icon/pre.svg' class='img-fluid w-75'></button>",
        nextArrow:"<button type='button' class='custommer_slick_arrows slick-arrow slick-next'><img src='images/icon/next.svg' class='img-fluid w-75'></button>",
        responsive: [
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 1
            }
        }
        ]
    });
</script>
@endpush
