@extends('front.layouts.app')
@section('content')
<section class="banner-wrapper" style="position: relative;">
    <img src="front/images/banner/category.jpg" class="img-fluid" alt="">
    <div class="heading--sub carousel-caption text-capitalize d-none d-md-block">
        {{ $data->title }}
    </div>
</section>
@include('front.blocks.breadcrumb')
<main role="main" class="article-page">
    <section class="page--content--wrapper">
        <div class="container">
            <div class="page__desc">
                {!! $data->description !!}
            </div>
            <div class="page__content">
                {!! $data->content !!}
            </div>
        </div>
    </section>
</main>
@endsection

@push('scripts')
<script>
    function initMap() {
        var center = { lat: 10.7908363, lng: 106.7500076 };
        var company = { lat: 10.7908363, lng: 106.7500076 };
        var map = new google.maps.Map(document.getElementById('company_map'), {
            zoom: 14,
            center: center
        });
        var marker = new google.maps.Marker({
            position: company,
            map: map,
            animation: google.maps.Animation.BOUNCE
        });

        var infowindow = new google.maps.InfoWindow({
            content: "Centana Tower A11.3, 36 Mai Chi Tho st., An Phu Ward, Dist.2, HCM City, VN"
        });

        infowindow.open(map, marker);
    }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAv1n051CAoSfbEJmjQCDZNsQLxODMIwpE&callback=initMap&language=vi-VN">
    </script>
@endpush
