@extends('front.layouts.app')
@section('content')
<section class="banner-wrapper" style="position: relative;">
    <img src="front/images/banner/category.jpg" class="img-fluid" alt="">
    <div class="news-child-page heading--sub carousel-caption text-capitalize d-none d-md-block">
        {{ $data->name }}
    </div>
</section>
@include('front.blocks.breadcrumb')
<main role="main" class="news-child-page">
    <section class="page--content--wrapper">
        <div class="container">
            <div class="page__desc">
                {!! $data->description !!}
            </div>
            <div class="page__content">
                {!! $data->content !!}
            </div>
        </div>
    </section>
</main>
@endsection
