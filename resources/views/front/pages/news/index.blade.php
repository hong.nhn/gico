@extends('front.layouts.app')
@section('content')
<section class="banner-wrapper" style="position: relative;">
    <img src="front/images/banner/category.jpg" class="img-fluid" alt="">
    <div class="heading--sub carousel-caption text-capitalize d-none d-md-block">
        {{ $data['title'] }}
    </div>
</section>
@include('front.blocks.breadcrumb')
<main role="main" class="news-page">
    <section class="page--content--wrapper">
        <div class="container">
            <div class="page--content">
                <div class="row">
                    <div class="col-md-4 sidebar">
                        <div class="widget">
                            <div class="widget-title gico-bg">Mới Cập Nhật</div>
                            <div class="recent-posts-widget">
                                @foreach ($data['updatedList'] as $item)
                                <article class="entry clearfix">
                                    <div class="entry-media">
                                        <a href="{{ $item->node->slug }}">
                                            <img src="{{ asset($item->image_path) }}" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="entry-title">
                                        <a href="{{ $item->node->slug }}" class="gico-color">{{ $item->name }}</a>
                                    </div>
                                    <div class="entry-meta">{{ NEWS_UPDATED_CREATED_AT_TITLE . $item->time }}</div>
                                </article>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        @foreach ($data['list'] as $item)
                        <article class="entry">
                            <div class="entry-media">
                                <figure>
                                    <a href="{{ $item->node->slug }}">
                                        <img src="{{ asset($item->image_path) }}" class="img-fluid">
                                    </a>
                                </figure>
                            </div>
                            <div class="entry-wrapper">
                                <div class="entry-title text-capitalize">
                                    <a href="{{ $item->node->slug }}" class="gico-color">{{ $item->name }}</a>
                                </div>
                                <div class="entry-content">
                                    {!! $item->description !!}
                                </div>
                            </div>
                        </article>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
