<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
            <a href="{{ route('admin.menu.index') }}" class="nav-link">
                <i class="nav-icon fas fa-bars"></i>
                <p>Menu</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.app_text.index') }}" class="nav-link">
                <i class="nav-icon fas fa-remove-format"></i>
                <p>App Text</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.solution.index') }}" class="nav-link">
                <i class="nav-icon fas fa-sitemap"></i>
                <p>Giải pháp</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.production.index') }}" class="nav-link">
                <i class="nav-icon fas fa-sitemap"></i>
                <p>Sảm phẩm</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.news.index') }}" class="nav-link">
                <i class="nav-icon fas fa-sitemap"></i>
                <p>Tin tức</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.article.index') }}" class="nav-link">
                <i class="nav-icon fas fa-pager"></i>
                <p>Bài viết</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.image-upload.index') }}" class="nav-link">
                <i class="nav-icon fas fa-images"></i>
                <p>Hình ảnh</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.customer_rating.index') }}" class="nav-link">
                <i class="nav-icon fas fa-pager"></i>
                <p>KH Đánh giá</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('admin.construction.index') }}" class="nav-link">
                <i class="nav-icon fas fa-pager"></i>
                <p>CTrình TBiêu</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('password.change') }}" class="nav-link">
                <i class="nav-icon fas fa-key"></i>
                <p>Mật khẩu</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                <p>Đăng xuất</p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>


