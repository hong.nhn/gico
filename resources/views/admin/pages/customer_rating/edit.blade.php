@extends('admin.layouts.app')

@section('title', 'Đánh giá từ khách hàng')

@push('css')
<link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
<style>
    .note-group-select-from-files {
        display: none;
    }
</style>
@endpush

@section('breadcrumb')
<li class="breadcrumb-item active">Đánh giá từ khách hàng</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Chỉnh sửa</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.customer_rating.update', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên</label>
                        <input type="text" class="form-control" name="name" value="{{ $data->name }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Star</label>
                        <input type="number" min="1" max="5" class="form-control" name="star" value="{{ $data->star }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nội dung</label>
                        <textarea rows="5" class="form-control show_edit_tool" name="content" required>{!! $data->content !!}</textarea>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Cập nhật vị trí</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.customer_rating.update.sort', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center">
                        <input type="number" min="1" max="40" class="form-control" name="sort" value="{{ $data->sort }}">
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
        <div class="card card-danger mt-5">
            <div class="card-header">
                <h3 class="card-title">Cập nhật trạng thái</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.customer_rating.update.active', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center">
                        <input type="checkbox" class="form-control" name="active" @if ($data->active == config('desf.admin.active') ) checked @endif>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
    $(function () {
        $('.show_edit_tool').summernote({
          toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['view', ['codeview']],
          ]
        });
    })
</script>

<script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
    $('input[name="active"]').bootstrapSwitch();
</script>

@endpush
