@extends('admin.layouts.app')
@section('title', 'Đánh giá từ khách hàng')
@section('breadcrumb')
<li class="breadcrumb-item active">Đánh giá từ khách hàng</li>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">DataTable</h3>
                <div class="float-right">
                    <a class="btn btn-app" href="{{ route('admin.customer_rating.create') }}">
                        <i class="fa fa-play"></i> Thêm mới
                    </a>
                </div>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="data_table" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc">#</th>
                                        <th class="sorting">Tên</th>
                                        <th class="sorting">Vị trí</th>
                                        <th class="sorting">Trạng thái</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($datas))
                                    @foreach ($datas as $key => $item)
                                    <tr role="row" class="odd">
                                        <td tabindex="0" class="sorting_1">{{ $key + 1 }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->sort }}</td>
                                        <td>
                                            <button type="button" class="btn btn-sm btn-{{ $item->active_class }} disabled">{{ $item->active_title }}</button>
                                        </td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="{{ route('admin.customer_rating.edit', $item->id) }}">
                                                <i class="fas fa-pencil-alt"></i>
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="4" class="text-center">Không có dữ liệu</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection

@push('scripts')
<script>
  $(function () {
    $('#data_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush
