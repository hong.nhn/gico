@extends('admin.layouts.app')

@section('title', 'Tin tức')

@push('css')
<link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
<style>
    .note-group-select-from-files {
        display: none;
    }
</style>
@endpush

@section('breadcrumb')
<li class="breadcrumb-item active">Tin tức</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Chỉnh sửa</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.news.update', $data->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên</label>
                        <input type="text" class="form-control" name="name" value="{{ $data->name }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mô tả</label>
                            <textarea rows="5" class="form-control show_edit_tool" name="description">{!! $data->description; !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nội dung</label>
                        <textarea rows="5" class="form-control show_edit_tool" name="content">{!! $data->content !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hình ảnh(770px 370px)</label>
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ asset($data->image_path) }}" class="img-fluid">
                            </div>
                            <div class="col-md-8 align-middle">
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="image_file" class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Chọn hình</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Cập nhật vị trí</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.news.update.sort', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center">
                        <input type="number" min="1" max="40" class="form-control" name="sort" value="{{ $data->sort }}">
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
        <div class="card card-danger mt-5">
            <div class="card-header">
                <h3 class="card-title">Cập nhật trạng thái</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.news.update.active', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center">
                        <input type="checkbox" class="form-control" name="active" @if ($data->active == config('desf.admin.active') ) checked @endif>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
    $(function () {
        $('.show_edit_tool').summernote({
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    document.execCommand('insertText', false, bufferText);
                }
            }
        });
    })
</script>

<script>
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>

<script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
    $('input[name="active"]').bootstrapSwitch();
</script>

@endpush
