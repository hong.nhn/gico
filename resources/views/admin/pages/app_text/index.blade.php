@extends('admin.layouts.app')
@section('title', 'App Text')
@section('breadcrumb')
<li class="breadcrumb-item active">App Text</li>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">DataTable</h3>
                <div class="float-right">
                    <a class="btn btn-app" href="{{ route('admin.app_text.create') }}">
                        <i class="fa fa-play"></i> Thêm mới
                    </a>
                </div>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="data_table" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc">#</th>
                                        <th class="sorting">ID</th>
                                        <th class="sorting">Title</th>
                                        <th class="sorting">Type</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($datas as $key => $item)
                                    <tr role="row" class="odd">
                                        <td tabindex="0" class="sorting_1">{{ $key + 1 }}</td>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ TEXT_TITLE[$item->type] }}</td>
                                        <td>
                                            <a class="btn btn-info btn-sm" href="{{ route('admin.app_text.edit', $item->id) }}">
                                                <i class="fas fa-pencil-alt"></i>
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection

@push('scripts')
<script>
  $(function () {
    $('#data_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush
