@extends('admin.layouts.app')
@section('title', 'App Text')
@section('breadcrumb')
<li class="breadcrumb-item active">App Text</li>
@endsection
@section('content')
<div class="row">
    <div class="col-md-6 offset-md-3">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Chỉnh sửa</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.app_text.update', $data->id) }}">
                @method('PUT')
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tiêu đề</label>
                        <input type="text" class="form-control" name="name" required value="{{ $data->name }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Link</label>
                        <input type="text" class="form-control" name="link" value="{{ $data->link }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Hình ảnh</label>
                        <input type="text" class="form-control" name="image_path" value="{{ $data->image_path }}">
                    </div>
                    <div class="form-group">
                        <label>Loại</label>
                        <select name="type" class="form-control" required>
                            @foreach (TEXT_TITLE as $key => $value)
                                <option value="{{ $key }}" @if ($key == $data->type) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
