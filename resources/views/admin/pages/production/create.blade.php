@extends('admin.layouts.app')
@section('title', 'Sản phẩm')
@push('css')
<link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
<style>
    .note-group-select-from-files {
        display: none;
    }
</style>
@endpush
@section('breadcrumb')
<li class="breadcrumb-item active">Sản phẩm</li>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Thêm mới</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.production.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên</label>
                        <input type="text" class="form-control" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mô tả</label>
                        <textarea rows="5" class="form-control show_edit_tool" name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nội dung</label>
                        <textarea rows="5" class="form-control show_edit_tool" name="content"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tính năng</label>
                        <textarea rows="5" class="form-control show_edit_tool" name="feature"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Thông số kỹ thuật</label>
                        <textarea rows="5" class="form-control show_edit_tool" name="technical"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Hình ảnh (221px x 308px)</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" required name="image_file" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Chọn hình</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
$(function() {
    $('.show_edit_tool').summernote({
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }
    });
});

</script>
<script>
// Add the following code if you want the name of the file appear on select
$(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

</script>
@endpush
