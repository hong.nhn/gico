@extends('admin.layouts.app')

@section('title', 'Bài viết')

@push('css')
<link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
<style>
    .note-group-select-from-files {
        display: none;
    }
</style>
@endpush

@section('breadcrumb')
<li class="breadcrumb-item active">Bài viết</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Chỉnh sửa</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.article.update', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên</label>
                        <input type="text" class="form-control" name="name" value="{{ $data->name }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tiêu đề</label>
                        <input type="text" class="form-control" name="title" value="{{ $data->title }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mô tả</label>
                            <textarea rows="5" class="form-control show_edit_tool" name="description">{!! $data->description; !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nội dung</label>
                        <textarea rows="5" class="form-control show_edit_tool" name="content">{!! $data->content !!}</textarea>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Cập nhật Menu</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.article.update.menu', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center">
                        <select name="menu_id" class="form-control">
                            <option value=""></option>
                            @foreach ($menus as $menu)
                            <option @if ($menu->ref_id == $data->id) selected @endif value="{{ $menu->id }}">{{ $menu->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
        <div class="card card-danger mt-5">
            <div class="card-header">
                <h3 class="card-title">Cập nhật trạng thái</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.solution.update.active', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center">
                        <input type="checkbox" class="form-control" name="active" @if ($data->active == config('desf.admin.active') ) checked @endif>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
    $(function () {
        $('.show_edit_tool').summernote({
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }
    });
    })
</script>

<script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
    $('input[name="active"]').bootstrapSwitch();
</script>

@endpush
