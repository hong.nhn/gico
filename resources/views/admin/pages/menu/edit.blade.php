@extends('admin.layouts.app')

@section('title', 'Menu')

@section('breadcrumb')
<li class="breadcrumb-item active">Menu</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Chỉnh sửa</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.menu.update', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tiêu đề</label>
                        <input type="text" class="form-control" name="name" value="{{ $data->name }}" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Link</label>
                        <input type="text" class="form-control" name="slug" value="{{ $data->node->slug }}" required>
                    </div>
                    <div class="form-group">
                        <label>Loại</label>
                        <select name="type" class="form-control" required>
                            @foreach (TYPE_TITLE as $key => $value)
                                <option @if ($key == $data->type) selected @endif value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                  </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Hiển thị Header</h3>
            </div>
            <form method="POST" role="form" action="{{ route('admin.menu.update.top', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center">
                        <input type="checkbox" class="form-control" name="top" @if ($data->show_top == config('desf.admin.active') ) checked @endif>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
        <div class="card card-info mt-5">
            <div class="card-header">
                <h3 class="card-title">Hiển thị Footer</h3>
            </div>
            <form method="POST" role="form" action="{{ route('admin.menu.update.footer', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center">
                        <input type="checkbox" class="form-control" name="footer" @if ($data->show_footer == config('desf.admin.active') ) checked @endif>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title">Cập nhật vị trí</h3>
            </div>
            <form method="POST" role="form" action="{{ route('admin.menu.update.sort', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center mb-2">
                        <input type="number" min="1" max="40" class="form-control" name="sort" value="{{ $data->sort }}">
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
        <div class="card card-danger mt-5">
            <div class="card-header">
                <h3 class="card-title">Cập nhật trạng thái</h3>
            </div>
            <form method="POST" role="form" action="{{ route('admin.menu.update.active', $data->id) }}">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group text-center">
                        <input type="checkbox" class="form-control" name="active" @if ($data->active == config('desf.admin.active') ) checked @endif>
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script>
    $('input[type="checkbox"]').bootstrapSwitch();
</script>

@endpush
