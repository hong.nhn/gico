@extends('admin.layouts.app')

@section('title', 'Hình Ảnh')

@push('css')
<link rel="stylesheet" href="{{ asset('admin/plugins/fancybox/css/jquery.fancybox.min.css') }}">
@endpush

@section('breadcrumb')
<li class="breadcrumb-item active">Hình Ảnh</li>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">DataTable</h3>
                <div class="float-right">
                    <a class="btn btn-app" href="{{ route('admin.image-upload.create') }}">
                        <i class="fa fa-play"></i> Thêm mới
                    </a>
                </div>

            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"></div>
                        <div class="col-sm-12 col-md-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="data_table" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc">#</th>
                                        <th class="sorting">Hình ảnh</th>
                                        <th class="sorting">Full Link <small>Sử dụng cho phần nội dung</small>  </th>
                                        <th>View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (isset($datas))
                                    @foreach ($datas as $key => $item)
                                    <tr role="row" class="odd">
                                        <td tabindex="0" class="sorting_1">{{ $key + 1 }}</td>
                                        <td>
                                            <img src="{{ asset($item->image_path) }}" style="width: 70px;">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" value="{{ $item->image_path_full }}">
                                        </td>
                                        <td><a href="{{ asset($item->image_path) }}" data-toggle="fancybox" class="btn btn-info"><i class="fas fa-expand-alt"></i></a></td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="4" class="text-center">Không có dữ liệu</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
@endsection

@push('scripts')

<script src="{{ asset('admin/plugins/fancybox/js/jquery.fancybox.min.js') }}"></script>
<script>
    $(function () {
        $('a[data-toggle="fancybox"]').fancybox();
    })
</script>
<script>
  $(function () {
    $('#data_table').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
@endpush
