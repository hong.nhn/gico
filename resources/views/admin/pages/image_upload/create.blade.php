@extends('admin.layouts.app')

@section('title', 'Hình Ảnh')

@push('css')
<link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">111

@endpush

@section('breadcrumb')
<li class="breadcrumb-item active">Hình Ảnh</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Thêm mới</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form method="POST" role="form" action="{{ route('admin.image-upload.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                    <label for="exampleInputFile">Hình ảnh</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" required name="image_file" class="custom-file-input" required>
                        <label class="custom-file-label" for="exampleInputFile">Chọn hình</label>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Hoàn thành</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>



@endpush
